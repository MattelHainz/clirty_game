﻿using System;
using UnityEngine;

namespace Clirty
{
    public class DirtMesh
    {
        Mesh mesh;
        private int width;
        private int height;
        private float xLeft;
        private float xRight;
        private float yTop;
        private float yBottom;

        private float tileWidth;
        private float tileHeight;

        private Vector3[] GetVertices()
        {
            Vector3[] vertices = new Vector3[width * height];
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    float xPos = xLeft + (x + 0.5f) * tileWidth;
                    float yPos = yTop - (y + 0.5f) * tileHeight;
                    Vector3 position = new Vector3(xPos, 0, yPos);
                    int index = GetVertexIndex(x, y);
                    vertices[index] = position;
                }
            }
            return vertices;
        }

        private int GetVertexIndex(int x, int y)
        {
            return x * height + y;
        }

        private Vector2[] GetUV()
        {
            Vector2[] uv = new Vector2[width * height];
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    float u = ((float)x) / width;
                    float v = ((float)y) / height;
                    Vector2 uvTemp = new Vector2(u, v);
                    int index = GetVertexIndex(x, y);
                    uv[index] = uvTemp;
                }
            }
            return uv;
        }

        private Vector3[] GetNormals()
        {
            Vector3[] normals = new Vector3[width * height];
            for(int i = 0; i < width * height; i++)
            {
                normals[i++] = Vector3.up;
            }
            return normals;
        }

        private int[] GetTriangles()
        {
            int[] triangles = new int[width * height * 2 * 3];
            int index = 0;
            for(int x = 0; x < (width - 1); x++)
            {
                for(int y = 0; y < (height - 1); y++)
                {
                    // Triangle 1
                    triangles[index++] = GetVertexIndex(x + 1, y + 1);
                    triangles[index++] = GetVertexIndex(x + 1, y);
                    triangles[index++] = GetVertexIndex(x, y);
                    // Triangle 2
                    triangles[index++] = GetVertexIndex(x, y + 1);
                    triangles[index++] = GetVertexIndex(x + 1, y + 1);
                    triangles[index++] = GetVertexIndex(x, y);
                }
            }
            return triangles;
        }

        private void ConstructMesh()
        {
            /*Vector3[] vertices = new Vector3[]
            {
                new Vector3(0, 0, 0),
                new Vector3(0, 0, 10),
                new Vector3(10, 0, 0),
                new Vector3(10, 0, 10),
            };
            Vector2[] uv = new Vector2[] {
                new Vector2(0, 0),
                new Vector2(0, 1),
                new Vector2(1, 0),
                new Vector2(1, 1),
            };
            int[] triangles = new int[]{
                0, 1, 3,
                0, 3, 2,
            };*/
            mesh = new Mesh
            {
                indexFormat = UnityEngine.Rendering.IndexFormat.UInt32,
                vertices = GetVertices(),
                uv = GetUV(),
                normals = GetNormals(),
                triangles = GetTriangles(),
            };
        }

        public Mesh GetMesh()
        {
            return mesh;
        }

        public DirtMesh(int width, int height, float xLeft, float xRight, float yTop, float yBottom)
        {
            this.width = width;
            this.height = height;
            this.xLeft = xLeft;
            this.xRight = xRight;
            this.yTop = yTop;
            this.yBottom = yBottom;

            tileWidth = (xRight - xLeft) / width;
            tileHeight = (yTop - yBottom) / height;

            ConstructMesh();
        }
    }
}