using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScript : MonoBehaviour
{

    [SerializeField] AudioClip[] audioClips;

    AudioSource audioSource;


    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        //for(int i = 0; i < audioClips.Length; i++)
        //{
        //    Debug.Log("Array length = " + i);
        //    Debug.Log(audioClips[i].name);
        //}
    }

    void Update()
    {
      // playBombSplash();
    }

    public void playBombCharge()
    {
        audioSource.PlayOneShot(audioClips[0], 0.5F);
        //Debug.Log("BombChargeSoundPlay");
    }

    public void playBombSplash()
    {
        audioSource.PlayOneShot(audioClips[1], 0.5F);
        //Debug.Log("BombSplashSoundPlay");
    }

    public void playBombThrow()
    {
        audioSource.PlayOneShot(audioClips[2], 0.5F);
    }

    public void playButtonClickMiniGame()
    {
        audioSource.PlayOneShot(audioClips[3], 0.5F);
    }

    public void playCloseDoor()
    {
        audioSource.PlayOneShot(audioClips[4], 0.5F);
    }

    public void playDestroyWoodstuff()
    {
        audioSource.PlayOneShot(audioClips[5], 0.5F);
    }

    public void playOpenDoor()
    {
        audioSource.PlayOneShot(audioClips[6], 0.5F);
    }

    public void playRepair()
    {
        audioSource.PlayOneShot(audioClips[7], 0.5F);
    }

    public void playWaterhose()
    {
        audioSource.PlayOneShot(audioClips[8], 0.5F);
    }
}
