using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Clirty
{
    public class Grenade : NetworkBehaviour
    {
        [SerializeField] private float time;
        private float actualTime;
        [SerializeField] private float range;
        private float actualRange;
        [SerializeField] public float explosionRadius;
        [SerializeField] private float gravity;
        [SerializeField] private float fullChargeTime = 1;
        [SerializeField] private float stunTime = 5;

        [SerializeField] private GameObject explosionParticleSystem;

        Vector3 direction;

        public GameObject thrower;

        public float chargeTime;
        //[SerializeField] private ParticleSystem particleSystem;

        public MeshRenderer meshRenderer;
        public BoolGrid grid;

        private float horizontalSpeed;
        private float verticalSpeed;

        public bool initialized;
        private bool exploded;

        Vector3[] flightPath;
        float startTime;
        float rotationSpeed = 360;

        public bool thrown;

        public bool HasFlightPath()
        {
            return flightPath != null;
        }

        private void ComputeFlightTimeRangeAndSpeed(float chargeTime)
        {
            float charge = chargeTime / fullChargeTime;
            charge = charge > 1 ? 1 : charge;
            actualTime = time * charge;
            actualTime = actualTime <= 0 ? 0.001f : actualTime;
            actualRange = range * charge;
            horizontalSpeed = actualRange / actualTime;
            verticalSpeed = -(transform.position.y - gravity * 0.5f * actualTime * actualTime) / actualTime;
        }

        private Vector3 ComputeNewPosition(Vector3 oldPosition, float timeStep)
        {
            Vector3 newPosition = oldPosition + horizontalSpeed * direction * timeStep;
            newPosition.y += verticalSpeed * timeStep;
            verticalSpeed -= gravity * timeStep;
            return newPosition;
        }

        public Vector3[] GetIndicatorLinePoints(Vector3 start, Vector3 direction, float chargeTime)
        {
            int count = 100;
            var points = new Vector3[count];

            ComputeFlightTimeRangeAndSpeed(chargeTime);
            this.direction = direction;

            float step = actualTime / (count - 1);
            points[0] = start;
            for(int i = 1; i < count; i++)
            {
                points[i] = ComputeNewPosition(points[i - 1], step);
            }

            flightPath = points;

            return points;
        }

        // Start is called before the first frame update
        void Start()
        {
           // bombExplodeSound = GetComponent<AudioSource>();

            meshRenderer = GetComponentInChildren<MeshRenderer>();
            meshRenderer.enabled = false;
            if (!isServer)
            {
                return;
            }
            ComputeFlightTimeRangeAndSpeed(chargeTime);
        }

        [ClientRpc]
        private void EnableRenderer()
        {
            if(meshRenderer == null)
            {
                meshRenderer = GetComponentInChildren<MeshRenderer>();
            }
            meshRenderer.enabled = true;
        }

        // Update is called once per frame
        void Update()
        {
            if (!isServer || exploded)
            {
                return;
            }
            if (!thrown)
            {
                return;
            }
            if (!initialized)
            {
                startTime = Time.time;
            }
            meshRenderer.enabled = true;
            EnableRenderer();

            initialized = true;
            float elapsedTime = Time.time - startTime;
            float indexFloat = elapsedTime / actualTime * (flightPath.Length - 1);
            if(indexFloat >= flightPath.Length - 1)
            {
                Explode();
                return;
            }
            int index = (int)indexFloat;
            transform.position = Vector3.Lerp(flightPath[index], flightPath[index + 1], indexFloat - index);
            transform.Rotate(transform.right, Time.deltaTime * rotationSpeed);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!isServer || !initialized || other.gameObject == thrower || other.gameObject.TryGetComponent(out Grenade o))
            {
                return;
            }
            Explode();
        }

        [ClientRpc]
        private void StartParticlesRpc(Vector3 position)
        {
            StartParticles(position);
        }

        private void StartParticles(Vector3 position)
        {
            var explosion = Instantiate(explosionParticleSystem);
            explosion.transform.position = position;
        }

        void Explode()
        {
            StartParticlesRpc(transform.position);
            StartParticles(transform.position);
            var players = MatchManager.instance.teamClean;
            foreach (var player in players)
            {
                if((transform.position - player.transform.position).magnitude < explosionRadius)
                {
                    player.Stun(stunTime);
                }
            }
            exploded = true;
            grid.DrawCircleServer(new Vector2(transform.position.x, transform.position.z), explosionRadius, true);
           // bombExplodeSound.Play(0);
            //particleSystem.Play();
            //Destroy(gameObject);
            NetworkServer.Destroy(gameObject);
        }
    }
}
