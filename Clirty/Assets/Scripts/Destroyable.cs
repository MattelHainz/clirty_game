using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;

public class Destroyable : NetworkBehaviour
{
    [SyncVar]
    public bool dirty = false;

    public GameObject cleanO;
    public GameObject dirtyO;
    public override void OnStartClient()
    {
        base.OnStartClient();
        UpdateLook();
    }

    public override void OnStartServer()
    {

        base.OnStartServer();
        int rng = Random.Range(0, 2);
        if (rng == 0)
        {
            dirty = false;
            //Debug.Log("Random Prop Set to Clean");
        }
        else
        {
            dirty = true;
            //Debug.Log("Random Prop Set to Dirty");
        }
    }
    

    public void MakeDirty()
    {
        NotifyStateChange(true);
    }

    public void MakeClean()
    {
        NotifyStateChange(false);
    }

    [Command(requiresAuthority = false)]
    public void NotifyStateChange(bool newDirty)
    {
        dirty = newDirty;
        RpcUpdateDirtState(newDirty);
    }

    [ClientRpc]
    public void RpcUpdateDirtState(bool newDirty)
    {
        Debug.Log("Received rpc");
        dirty = newDirty;
        UpdateLook();
    }

    private void UpdateLook()
    {
        /*foreach (Transform child in transform)
        {
            if (child.tag == "Dirty")
                child.gameObject.SetActive(dirty);
            if (child.tag == "Clean")
                child.gameObject.SetActive(!dirty);
        }*/
        try
        {
            dirtyO.SetActive(dirty);
            cleanO.SetActive(!dirty);
        }
        catch(Exception e)
        {
            Debug.Log(this.gameObject.name + " failed to update looks with: " + e.Message);
        }
    }
    
}
