using System;
using System.Collections;
using System.Collections.Generic;
using Clirty;
using Mirror;
using UnityEngine;
using UnityEngine.UI;

public class ShelfBehavior : NetworkBehaviour
{
    [SerializeField] GameObject[] books;
    [SerializeField] float radius;
    private List<GameObject> content = new List<GameObject>();

    public GameObject cleanVersion, dirtyVersion;

    public bool dirty = false;
    [SerializeField] string flag = "";

    // Start is called before the first frame update
    void Start()
    {
        cleanVersion.SetActive(true);
        dirtyVersion.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    [Command(requiresAuthority = false)]
    public void MakeAllDirty()
    {
        MakeDirtyLocal();
        MakeDirty();
    }

    [ClientRpc]
    private void MakeDirty()
    {
        if(GetComponent<AudioScript>() != null)
        {
            if(flag.Equals("shelf"))
                GetComponent<AudioScript>().playOpenDoor();
            if (flag.Equals("chest"))
                GetComponent<AudioScript>().playOpenDoor();
            if (flag.Equals("bookshelf"))
                GetComponent<AudioScript>().playDestroyWoodstuff();
        }

        MakeDirtyLocal();
    }

    private void MakeDirtyLocal()
    {
        if (!dirty)
        {
            cleanVersion.SetActive(false);
            dirtyVersion.SetActive(true);
            ShufflePrefabs();
            for (int j = 0; j < 4; j++)
            {
                Vector3 position = new Vector3(0, 0, 0);
                int i = UnityEngine.Random.Range(0, 2);
                if (i == 0)
                {
                    float forward = UnityEngine.Random.Range(.5f, 1.0f);
                    float sideward = UnityEngine.Random.Range(.0f, 1.0f - forward * forward);
                    position = this.transform.forward * forward * radius + this.transform.right * sideward * radius + transform.position;
                }
                else
                {
                    float forward = UnityEngine.Random.Range(.5f, 1.0f);
                    float sideward = UnityEngine.Random.Range(.0f, 1.0f - forward * forward);
                    position = this.transform.forward * forward * radius - this.transform.right * sideward * radius + transform.position;
                }
                position.y = 5;
                Vector3 rotation = new Vector3(UnityEngine.Random.Range(0, 90), UnityEngine.Random.Range(0, 90), UnityEngine.Random.Range(0, 90));
                GameObject book = Instantiate(books[j], position, Quaternion.Euler(rotation.x, rotation.y, rotation.z));
                book.GetComponent<BookBehavior>().source = this.gameObject;
                content.Add(book);
            }
            dirty = true;
        }
    }

    public bool PlayerHasAllObjects(Player player)
    {
        int numberOfObjects = 0;
        foreach(var bookBehavior in player.inventory){
            if(bookBehavior.source == gameObject)
            {
                numberOfObjects++;
            }
        }
        return numberOfObjects >= 4;
        /*int i = 0;
        GameObject[] objsToDelete = new GameObject[4];
        List<GameObject> objs = player.GetComponent<Player>().inventory;
        bool allobjs = false;
        minigame = false;
        for (int j = 0; j < objs.Count; j++)
        {
            if (objs[j].GetComponent<BookBehavior>().source == gameObject)
            {
                objsToDelete[i] = objs[j];
                i++;
            }
            if (i == 4)
            {
                for (int k = 0; k < 4; k++)
                {
                    if (objsToDelete[k].GetComponent<IsMatrojska>() != null)
                    {
                        minigame = true;
                    }
                    objs.Remove(objsToDelete[k]);
                    content.Remove(objsToDelete[k]);
                    Destroy(objsToDelete[k]);
                }
                player.GetComponent<Player>().inventory = objs;
                allobjs = true;
                break;
            }
        }
        return allobjs;*/
    }

    public bool MakeClean(Player player)
    {   
        if (!dirty)
        {
            return true;
        }
            
        if (!PlayerHasAllObjects(player))
        {
            return true;
        }

        if (flag.Equals("shelf"))
            GetComponent<AudioScript>().playCloseDoor();
        if (flag.Equals("chest"))
            GetComponent<AudioScript>().playCloseDoor();

        bool minigame = false;
        var objectsToDelete = new List<BookBehavior>();
        foreach(var bookBehavior in player.inventory)
        {
            if (bookBehavior.source != gameObject)
            {
                continue;
            }
            objectsToDelete.Add(bookBehavior);
            if(bookBehavior.TryGetComponent(out IsMatrojska ignoreOut))
            {
                minigame = true;
            }
        }
        CleanAllCupboards();
        foreach (var obj in objectsToDelete){
            player.inventory.Remove(obj);
            content.Remove(obj.gameObject);
            Destroy(obj.gameObject);
        }
            
        dirty = false;

        if(minigame)
            player.GetComponent<Player>().InitiateMiniGame();
        return minigame;
    }

    [ClientRpc]
    public void DeleteContents()
    {
        Debug.Log("Received call to delete contents");
        DeleteContentsLocal();
    }

    public void DeleteContentsLocal()
    {
        for(int i = 0; i < content.Count; )
        {
            Debug.Log(i + "th iteration");
            if (content[0].TryGetComponent(out BookBehavior bh))
            {
                Debug.Log("Got bookBehavior");
                bh.DespawnBeam();
            }
            Debug.Log("Destroying content");
            Destroy(content[0]);
            Debug.Log("Removing content");
            content.RemoveAt(0);
        }
        
        cleanVersion.SetActive(true);
        dirtyVersion.SetActive(false);
        dirty = false;
        Debug.Log("exiting DeleteContentsLocal");
    }

    [Command(requiresAuthority = false)]
    private void CleanAllCupboards()
    {
        DeleteContents();
    }

    private void ShufflePrefabs()
    {
        for (int i = 0; i < books.Length; i++)
        {
            int rnd = UnityEngine.Random.Range(0, books.Length);
            GameObject temp = books[rnd];
            books[rnd] = books[i];
            books[i] = temp;
        }
    }
}
