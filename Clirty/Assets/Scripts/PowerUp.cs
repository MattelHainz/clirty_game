using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class PowerUp : NetworkBehaviour
{
    Material material;
    [SerializeField] private float powerUpDuration;
    [SerializeField] private Color colorLeft;
    [SerializeField] private Color colorRight;
    [SerializeField] private float colorBlendTime;
    bool colorChangeRight;
    float colorBlend;

    // Start is called before the first frame update
    void Start()
    {
        material = GetComponent<MeshRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        if (colorChangeRight)
        {
            colorBlend += Time.deltaTime / colorBlendTime;
        } else
        {
            colorBlend -= Time.deltaTime / colorBlendTime;
        }
        if(colorBlend < 0 && !colorChangeRight || colorBlend > 1 && colorChangeRight)
        {
            colorChangeRight = !colorChangeRight;
        }
        material.SetColor("_BaseColor", Color.Lerp(colorLeft, colorRight, colorBlend * colorBlend));
    }

    public float GetPowerUpDuration()
    {
        return powerUpDuration;
    }
}
