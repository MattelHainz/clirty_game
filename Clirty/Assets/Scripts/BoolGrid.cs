using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Mirror;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Clirty
{
    [RequireComponent(typeof(MeshRenderer))]
    [RequireComponent(typeof(MeshFilter))]
    public class BoolGrid : NetworkBehaviour
    {
        [SerializeField] private int width;
        [SerializeField] private int height;
        private int cellCount;

        [SerializeField] private float xLeft;
        [SerializeField] private float xRight;
        [SerializeField] private float yBottom;
        [SerializeField] private float yTop;

        [SerializeField] private float seperationFactor = 8.0f;

        private float cellWidth;
        private float cellHeight;
        private float gridWidth;
        private float gridHeight;

        // True == dirty
        [SerializeField] public bool[,] grid;

        [SerializeField] Material material;
        MeshFilter meshFilter;

        DirtTexture dirtTexture;

        private int dirtScore;
        

        private void SetCell(int x, int y, bool dirty, float chance=1)
        {
            if (!IsInside(x, y))
            {
                //Debug.LogWarning("Index out of bounds of grid: " + x + ", " + y);
                return;
            }
            if(grid == null || dirtTexture == null)
            {
                Debug.LogWarning("The grid has not been properly initialized.");
                return;
            }
            if(grid[x,y] == dirty || Random.value > chance)
            {
                return;
            }
            AddScore(dirty);
            grid[x, y] = dirty;
            dirtTexture.SetPixel(x, y, dirty);
        }


        private void AddScore(bool dirty)
        {
            if (dirty)
            {
                dirtScore++;
            }
            else
            {
                dirtScore--;
            }
        }

        public float GetDirtPercentage()
        {
            return ((float)dirtScore) / cellCount;
        }

        private void SetCell(Vector2 position, bool dirty, float chance)
        {
            if (!IsInside(position))
            {
                //Debug.LogWarning("Index out of bounds of grid: " + position.x + ", " + position.y);
                return;
            }
            int x = (int)((position.x - xLeft) / cellWidth);
            int y = (int)((position.y - yBottom) / cellHeight);

            SetCell(x,y,dirty, chance);
        }

        private float Sigmoid(float t)
        {
            return 1 / (1 + Mathf.Exp(-t));
        }

        private IEnumerator InitializeFieldCoroutine()
        {
            Random.InitState(32443256);
            uint dirtyCount = 0;
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    float pos = x - width / 2.0f;
                    if (Random.value > Sigmoid(pos / width * seperationFactor))
                    {
                        dirtyCount++;
                        SetCell(x, y, true);
                    } else
                    {
                        SetCell(x, y, false);
                    }
                }
                if (x % 3 == 0)
                {
                    yield return null;
                }
            }
            BalanceField(dirtyCount);
        }

        private void BalanceField(uint dirtyCount)
        {
            int totalCount = width * height;
            while (dirtyCount < totalCount * 0.5f)
            {
                int x = (int)(Random.value * (width - 1));
                int y = (int)(Random.value * (height - 1));
                if (!grid[x, y])
                {
                    SetCell(x, y, true);
                    dirtyCount++;
                }
            }
            while (dirtyCount > totalCount * 0.5f)
            {
                int x = (int)(Random.value * (width - 1));
                int y = (int)(Random.value * (height - 1));
                if (grid[x, y])
                {
                    SetCell(x, y, false);
                    dirtyCount--;
                }
            }
        }

        private bool IsInside(Vector2 point)
        {
            return point.x >= xLeft && point.x < xRight && point.y >= yBottom && point.y < yTop;
        }

        private bool IsInside(int x, int y)
        {
            return x >= 0 && x < width && y >= 0 && y < height;
        }

        private float GetT(float target, float start, float direction)
        {
            return (target - start) / direction;
        }

        private Vector2 IntersectWithX(Vector2 point, Vector2 direction, float xTarget)
        {
            var t = GetT(xTarget, point.x, direction.x);
            var y = point.y + t * direction.y;
            return new Vector2(xTarget, y);
        }

        private Vector2 IntersectWithY(Vector2 point, Vector2 direction, float yTarget)
        {
            var t = GetT(yTarget, point.y, direction.y);
            var x = point.x + t * direction.x;
            return new Vector2(x, yTarget);
        }

        private bool CircleInsideGrid(Vector2 center, float radius)
        {
            return center.x + radius >= xLeft && center.x - radius <= xRight && center.y + radius >= yBottom && center.y - radius <= yTop;
        }

        private void SetCellsX(Vector2 center, float dx, float dy, bool dirty, float chance)
        {
            SetCell(new Vector2(center.x - dx, center.y - dy), dirty, chance);
            SetCell(new Vector2(center.x - dx, center.y + dy), dirty, chance);
            SetCell(new Vector2(center.x + dx, center.y - dy), dirty, chance);
            SetCell(new Vector2(center.x + dx, center.y + dy), dirty, chance);
        }

        public void DrawCircleServer(Vector2 center, float radius, bool dirty)
        {
            if (!isServer)
            {
                throw new Exception("This method should only be called by the server.");
            }
            float duration = 0.15f;
            DrawCircleRpc(center, radius, dirty, duration);
            StartCoroutine("DrawCircleCoroutine", new object[] {center, radius, dirty, duration});
        }

        [ClientRpc]
        private void DrawCircleRpc(Vector2 center, float radius, bool dirty, float duration)
        {
            StartCoroutine("DrawCircleCoroutine", new object[] { center, radius, dirty, duration});
        }

        private void DrawCircle(Vector2 center, float radius, bool dirty, float minChance)
        {
            if (!CircleInsideGrid(center, radius))
            {
                return;
            }
            float radiusSq = radius * radius;

            float dy = 0;
            while (dy < radius)
            {
                float dySq = dy * dy;
                float halfWidth = Mathf.Sqrt(radiusSq - dySq);
                float dx = 0;
                while (dx < halfWidth)
                {
                    float dist = dx * dx + dy * dy;
                    float t = dist / radiusSq;
                    t *= t;
                    float chance = Mathf.Lerp(1, minChance, dist / radiusSq);
                    SetCellsX(center, dx, dy, dirty, chance);
                    dx += cellWidth;
                }
                dy += cellHeight;
            }
        }

        private IEnumerator DrawCircleCoroutine(object[] parameters)
        {
            Vector2 center = (Vector2)parameters[0];
            float radius = (float)parameters[1];
            bool dirty = (bool)parameters[2];
            float duration = (float)parameters[3];
            int n = Mathf.RoundToInt(duration / 0.01f);
            for(int i = 0; i < n; i++)
            {
                yield return new WaitForFixedUpdate();
                Random.InitState(42);
                float currentRadius = 1.0f * i / n * radius;
                DrawCircle(center, currentRadius, dirty, 0.1f);
                yield return new WaitForFixedUpdate();
            }
            Random.InitState(42);
            DrawCircle(center, radius, dirty, 0.1f);
            yield return null;
        }

        private void DrawLine(int x, int xEnd, int y, int yEnd, bool dirty)
        {
            int dx = Mathf.Abs(xEnd - x);
            int sx = x < xEnd ? 1 : -1;
            int dy = -Mathf.Abs(yEnd - y);
            int sy = y < yEnd ? 1 : -1;
            int err = dx + dy;
            int e2; /* error value e_xy */

            while (true)
            {
                if (IsInside(x, y))
                {
                    SetCell(x, y, dirty);
                }
                if (x == xEnd && y == yEnd)
                {
                    break;
                }
                e2 = 2 * err;
                if (e2 > dy)
                {
                    err += dy;
                    x += sx; /* e_xy+e_x > 0 */
                }
                else if (e2 < dx)
                {
                    err += dx;
                    y += sy; /* e_xy+e_y < 0 */
                }
            }
        }

        public void DrawLineServer(Vector2 startFloat, Vector2 endFloat, bool dirty)
        {
            if (!isServer)
            {
                throw new Exception("This method should only be called by the server.");
            }
            DrawLineRpc(startFloat, endFloat, dirty);
            DrawLine(startFloat, endFloat, dirty);
        }

        [ClientRpc]
        private void DrawLineRpc(Vector2 startFloat, Vector2 endFloat, bool dirty)
        {
            DrawLine(startFloat, endFloat, dirty);
        }

        private void DrawLine(Vector2 startFloat, Vector2 endFloat, bool dirty)
        {
            int x = (int)((startFloat.x - xLeft) / cellWidth);
            int xEnd = (int)((endFloat.x - xLeft) / cellWidth);
            int y = (int)((startFloat.y - yBottom) / cellHeight);
            int yEnd = (int)((endFloat.y - yBottom) / cellHeight);
            DrawLine(x, xEnd, y, yEnd, dirty);
        }

        private void DrawTrapezoid(int x, int yTop, int yBottom, int xEnd, int yEndTop, int yEndBottom, bool dirty)
        {
            if(x == xEnd)
            {
                return;
            }
            if(yTop < yBottom)
            {
                int temp = yTop;
                yTop = yBottom;
                yBottom = temp;
            }
            if(yEndTop < yEndBottom)
            {
                int temp = yEndTop;
                yEndTop = yEndBottom;
                yEndBottom = temp;
            }
            if(x > xEnd)
            {
                int temp = x;
                x = xEnd;
                xEnd = temp;
            }
            float mTop = ((float)yEndTop - yTop) / (xEnd - x);
            int signTop = mTop > 0 ? 1 : -1;
            float errTop = 0;
            float mBottom = ((float)yEndBottom - yBottom) / (xEnd - x);
            int signBottom = mBottom > 0 ? 1 : -1;
            float errBottom = 0;
            for(; x < xEnd; x++)
            {
                errTop += mTop;
                while(Mathf.Abs(errTop) > 1)
                {
                    errTop -= signTop;
                    yTop += signTop;
                }
                errBottom += mBottom;
                while(Mathf.Abs(errBottom) > 1)
                {
                    errBottom -= signBottom;
                    yBottom += signBottom;
                }
                for(int y = yBottom; y < yTop; y++)
                {
                    SetCell(x, y, dirty);
                }
            }
        }

        private void DrawTrapezoid(Vector2 left1, Vector2 left2, Vector2 right1, Vector2 right2, bool dirty)
        {
            int x = (int)((left1.x - xLeft) / cellWidth);
            int yUp = (int)((left1.y - yBottom) / cellHeight);
            int yDown = (int)((left2.y - yBottom) / cellHeight);
            int xEnd = (int)((right1.x - xLeft) / cellWidth);
            int yEndUp = (int)((right1.y - yBottom) / cellHeight);
            int yEndDown = (int)((right2.y - yBottom) / cellHeight);
            DrawTrapezoid(x, yUp, yDown, xEnd, yEndUp, yEndDown, dirty);
        }


        private void DrawTriangle(Vector2 p1, Vector2 p2, Vector2 p3, bool dirty)
        {
            List<Vector2> points = new List<Vector2>
            {
                p1,
                p2,
                p3
            };
            points.Sort((x1, x2) => x1.x <= x2.x ? -1 : 1);

            float xMiddle = points[1].x;
            float m = (points[2].y - points[0].y) / (points[2].x - points[0].x);
            float yMiddle = points[0].y + m * (points[1].x - points[0].x);
            Vector2 middle = new Vector2(xMiddle, yMiddle);
            DrawTrapezoid(points[0], points[0], points[1], middle, dirty);
            DrawTrapezoid(points[1], middle, points[2], points[2], dirty);
            
        }

        public void DrawSquareServer(Vector2 a, Vector2 b, Vector2 c, Vector2 d, bool dirty)
        {
            if (!isServer)
            {
                throw new Exception("This method should only be called by the server.");
            }
            DrawSquareRpc(a, b, c, d, dirty);
            DrawSquare(a, b, c, d, dirty);
        }

        public void DrawSquareServer(Vector2 a, Vector2 b, Vector2 c, Vector2 d, bool dirty, float duration)
        {
            if (!isServer)
            {
                throw new Exception("This method should only be called by the server.");
            }
            DrawSquareWithDurationRpc(a, b, c, d, dirty, duration);
            DrawSquare(a, b, c, d, dirty, duration);
        }

        [ClientRpc]
        private void DrawSquareRpc(Vector2 a, Vector2 b, Vector2 c, Vector2 d, bool dirty)
        {
            DrawSquare(a, b, c, d, dirty);
        }

        [ClientRpc]
        private void DrawSquareWithDurationRpc(Vector2 a, Vector2 b, Vector2 c, Vector2 d, bool dirty, float duration)
        {
            DrawSquare(a, b, c, d, dirty, duration);
        }

        private void DrawSquare(Vector2 a, Vector2 b, Vector2 c, Vector2 d, bool dirty, float duration)
        {
            object[] parameters = new object[] { a, b, c, d, dirty, duration };
            StartCoroutine("DrawSquareCoroutine", parameters);
        }

        Vector2 Lerp(Vector2 a, Vector2 b, float t)
        {
            float x = Mathf.Lerp(a.x, b.x, t);
            float y = Mathf.Lerp(a.y, b.y, t);
            return new Vector2(x, y);
        }

        private IEnumerator DrawSquareCoroutine(object[] parameters)
        {
            Vector2 a = (Vector2)parameters[0];
            Vector2 b = (Vector2)parameters[1];
            Vector2 c = (Vector2)parameters[2];
            Vector2 d = (Vector2)parameters[3];
            bool dirty = (bool)parameters[4];
            float duration = (float)parameters[5];

            float startTime = Time.time;
            Vector2 center = 0.25f * (a + b + c + d);

            while(Time.time < startTime + duration)
            {
                float t = (Time.time - startTime) / duration;
                DrawSquare(Lerp(center, a, t), Lerp(center, b, t), Lerp(center, c, t), Lerp(center, d, t), dirty);
                yield return null;
            }
            DrawSquare(a, b, c, d, dirty);
        }

        private void DrawSquare(Vector2 a, Vector2 b, Vector2 c, Vector2 d, bool dirty)
        {
            DrawTriangle(a, b, d, dirty);
            DrawTriangle(b, c, d, dirty);
        }

        public void DrawConeServer(Vector2 start, Vector2 direction, float range, float angle, float speed, bool dirty)
        {
            if (!isServer)
            {
                throw new Exception("This method should only be called by the server.");
            }
            DrawConeRpc(start, direction, range, angle, speed, dirty);
            DrawCone(start, direction, range, angle, speed, dirty);

        }

        [ClientRpc]
        private void DrawConeRpc(Vector2 start, Vector2 direction, float range, float angle, float speed, bool dirty)
        {
            DrawCone(start, direction, range, angle, speed, dirty);
        }

        private void DrawCone(Vector2 start, Vector2 direction, float range, float angle, float speed, bool dirty)
        {
            object[] parameters = new object[] { start, direction, range, angle, speed, dirty };
            StartCoroutine("DrawConeCoroutine", parameters);
        }

        IEnumerator DrawConeCoroutine(object[] parameters)
        {
            Vector2 start = (Vector2)parameters[0];
            Vector2 direction = (Vector2)parameters[1];
            float range = (float)parameters[2];
            float angle = (float)parameters[3];
            float speed = (float)parameters[4];
            bool dirty = (bool)parameters[5];
            direction = direction.normalized;
            Vector2 perpendicular = new Vector2(direction.y, -direction.x);
            float halfAngle = angle * 0.5f;
            float tanHalfAngle = Mathf.Tan(halfAngle);

            float startTime = Time.time;
            float duration = range / speed;
            while (Time.time - startTime <= duration)
            {
                float currentRange = (Time.time - startTime) / duration * range;
                Vector2 actualPerpendicular = perpendicular * tanHalfAngle * currentRange;
                DrawTriangle(start, start + direction * currentRange + actualPerpendicular, start + direction * currentRange - actualPerpendicular, dirty);
                yield return null;
            }
            Vector2 perp = perpendicular * tanHalfAngle * range;
            DrawTriangle(start, start + direction * range + perp, start + direction * range - perp, dirty);
        }


        public float GetDirtyPercentage()
        {
            int dirtyCount = 0;
            int totalCount = width * height;
            for(int x = 0; x < width; x++)
            {
                for(int y = 0; y < height; y++)
                {
                    if(grid[x, y])
                    {
                        dirtyCount++;
                    }
                }
            }
            return ((float) dirtyCount) / totalCount;
        }

        public void ResetField()
        {
            //grid = new bool[width, height];
            //dirtScore = 0;
            //dirtTexture = new DirtTexture(grid);
            StartCoroutine("InitializeFieldCoroutine");
            //material.SetTexture("_BaseMap", dirtTexture.GetTexture());
            //material.SetVector("_TexelSize", new Vector2(1.0f / width, 1.0f / height));
        }


        // Start is called before the first frame update
        void Start()
        {
            meshFilter = GetComponent<MeshFilter>();
            grid = new bool[width, height];
            cellCount = width * height;
            dirtTexture = new DirtTexture(grid);
            gridWidth = xRight - xLeft;
            gridHeight = yTop - yBottom;
            cellWidth = gridWidth / width;
            cellHeight = gridHeight / height;
            StartCoroutine("InitializeFieldCoroutine");
            material.SetTexture("_BaseMap", dirtTexture.GetTexture());
            material.SetVector("_TexelSize", new Vector2(1.0f / width, 1.0f / height));
            DirtMesh dirtMesh = new DirtMesh(width, height, xLeft, xRight, yBottom, yTop);
            meshFilter.mesh = dirtMesh.GetMesh();
        }

        // Update is called once per frame
        void Update()
        {
            dirtTexture.UpdateTexture();
        }
    }
}
