using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Clirty
{
    public class WaterHose : NetworkBehaviour
    {
        public Transform playerTransform;
        [SerializeField] private float minRange;
        [SerializeField] private float maxRange;
        [SerializeField] private float openingAngle;
        [SerializeField] private float stunTime;

        [SerializeField] private GameObject particleEmitterPrefab;

        public BoolGrid grid;

        // Start is called before the first frame update
        void Start()
        {
            openingAngle *= Mathf.Deg2Rad;
            if (!isServer)
            {
                return;
            }
            transform.position = playerTransform.position;
            transform.rotation = playerTransform.rotation;
            transform.position -= transform.forward * 0.3f;
            StartParticlesRpc(transform.position, transform.rotation, openingAngle, maxRange);
            float speed = StartParticles(transform.position, transform.rotation, openingAngle, maxRange);

            Vector2 start = new Vector2(transform.position.x, transform.position.z);
            Vector2 direction = new Vector2(transform.forward.x, transform.forward.z).normalized;
            Spray(start, direction, maxRange, speed);
        }

        [ClientRpc]
        private void StartParticlesRpc(Vector3 position, Quaternion rotation, float angle, float range)
        {
            StartParticles(position, rotation, angle, range);
        }

        private float StartParticles(Vector3 position, Quaternion rotation, float angle, float range)
        {
            var particleEmitter = Instantiate(particleEmitterPrefab).GetComponent<ParticleSystem>();
            particleEmitter.transform.position = position;
            particleEmitter.transform.rotation = rotation;
            var shape = particleEmitter.shape;
            shape.angle = angle * Mathf.Rad2Deg;

            var main = particleEmitter.main;
            var startLifetime = main.startLifetime;
            startLifetime = range / particleEmitter.main.startSpeed.constant;
            main.startLifetime = startLifetime;
            return particleEmitter.main.startSpeed.constant;
        }

        private void StunPlayers(Vector2 start, Vector2 direction, float range)
        {
            var players = MatchManager.instance.teamDirty;
            foreach (var player in players)
            {
                Vector2 toPlayer = new Vector2(player.transform.position.x, player.transform.position.z) - start;
                float angleToPlayer = Vector2.Angle(toPlayer, direction) * Mathf.Deg2Rad;
                if (angleToPlayer < openingAngle * 0.5f && toPlayer.magnitude <= range)
                {
                    player.Stun(stunTime);
                }
            }
        }

        private IEnumerator StunPlayersCoroutine(float[] startDirectionSpeed)
        {
            Vector2 start = new Vector2(startDirectionSpeed[0], startDirectionSpeed[1]);
            Vector2 direction = new Vector2(startDirectionSpeed[2], startDirectionSpeed[3]);
            float speed = startDirectionSpeed[4];

            float startTime = Time.time;
            float duration = maxRange / speed;
            float endTime = startTime + duration;
            while(Time.time < endTime)
            {
                StunPlayers(start, direction, maxRange * (Time.time - startTime) / duration);
                yield return null;
            }
            NetworkServer.Destroy(gameObject);
        }

        private void Spray(Vector2 start, Vector2 direction, float maxRange, float speed)
        {
            grid.DrawConeServer(start, direction, maxRange, openingAngle, speed, false);
            float[] startDirectionSpeed = new float[]
            {
                start.x,
                start.y,
                direction.x,
                direction.y,
                speed,
            };
            StartCoroutine("StunPlayersCoroutine", startDirectionSpeed);
        }
    }
}
