using System.Collections;
using System.Collections.Generic;
using Mirror;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInterface : MonoBehaviour
{
    public Canvas canvas;
    public GameObject readyIndicator;
    public Vector3 readyIndicatorOffset;
    public GameObject playerNameField;
    public Vector3 playerNameOffset;
    public TextMeshProUGUI playerNameText;

    public TextMeshProUGUI button0, button1, button2, button3;

    public GameObject mat0, mat1, mat1filled, mat2, mat2filled, mat3, mat3filled;
    public GameObject target0, target1, target2, target3;
    public GameObject miniGameParent;

    private Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        canvas.transform.rotation = new Quaternion();
        Vector3 pos = transform.position;
        Vector3 viewportPos = cam.WorldToViewportPoint(pos);
        viewportPos = Vector3.Scale(viewportPos, new Vector3(canvas.renderingDisplaySize.x, canvas.renderingDisplaySize.y));
        readyIndicator.transform.position = viewportPos + readyIndicatorOffset;
        playerNameField.transform.position = viewportPos + playerNameOffset;

        /*Vector2 screenPosition = new Vector2
        (
            viewport.x * this.mGameController.GUICanvasRect.sizeDelta.x,
            viewport.y * this.mGameController.GUICanvasRect.sizeDelta.y
        );
 
        // this is reversed because of the anchor we are using for this element
        screenPosition.y = -screenPosition.y;*/
    }

    public void SetName(string playerName)
    {
        playerNameText.text = playerName;
    }

    public void SetReady(bool isReady)
    {
        readyIndicator.SetActive(isReady);
    }

    public void SetMiniGameProgress(int progress)
    {
        mat0.SetActive(false);
        mat1.SetActive(false);
        mat2.SetActive(false);
        mat3.SetActive(false);
        
        mat1filled.SetActive(false);
        mat2filled.SetActive(false);
        mat3filled.SetActive(false);
        
        target0.SetActive(false);
        target1.SetActive(false);
        target2.SetActive(false);
        target3.SetActive(false);
        
        switch (progress)
        {
            case 0:
                target0.SetActive(true);
                mat0.SetActive(true);
                mat1.SetActive(true);
                mat2.SetActive(true);
                mat3.SetActive(true);
                break;
            case 1:
                target1.SetActive(true);
                mat1filled.SetActive(true);
                mat2.SetActive(true);
                mat3.SetActive(true);
                break;
            case 2:
                target2.SetActive(true);
                mat2filled.SetActive(true);
                mat3.SetActive(true);
                break;
            case 3:
                target3.SetActive(true);
                mat3filled.SetActive(true);
                break;

        }
    }

    public void SetButtonText(string[] buttons)
    {
        button0.text = buttons[0];
        button1.text = buttons[1];
        button2.text = buttons[2];
        button3.text = buttons[3];
    }

    public void SetMiniGameVisible(bool isVisible)
    {
        miniGameParent.SetActive(isVisible);
    }
}
