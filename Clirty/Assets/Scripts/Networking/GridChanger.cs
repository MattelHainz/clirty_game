using System.Collections;
using System.Collections.Generic;
using Clirty;
using Mirror;
using UnityEngine;
using Grid = UnityEngine.Grid;

public class GridChanger : NetworkBehaviour
{
    // Start is called before the first frame update
    private BoolGrid grid;
    private float coolDown = 0;
    private bool lastDirty;

    void Start()
    {
        grid = GetComponent<BoolGrid>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isServer)
        {
            coolDown -= Time.deltaTime;
            if (coolDown <= 0)
            {
                coolDown = 3.0f;
                //grid.DrawCircle(new Vector2(0, 0), 2, lastDirty);
                lastDirty = !lastDirty;
            }
        }
    }
}