using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Clirty;
using Mirror;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;
using Random = Unity.Mathematics.Random;

public class MatchManager : NetworkBehaviour
{
    public GameObject dirtSpawn, cleanSpawn, lobbySpawn;
    public static MatchManager instance;
    public int teamCleanMinSize;
    public int teamDirtMinSize;
    [SyncVar] public double matchStart;
    public float matchDuration;
    [SerializeField] private float countDownDuration;
    [SyncVar] private float countDownToMatchStart = -1;
    public bool isMatchRunning;
    public List<Player> lobbyPlayers = new List<Player>();
    public List<Player> teamDirty = new List<Player>();
    public List<Player> teamClean = new List<Player>();
    public CameraController cameraController;

    public Color normalTimerColor;
    public Color almostOverTimerColor;
    public TextMeshProUGUI timer;

    [SerializeField] private Transform powerUpSpawnPoints;
    [SerializeField] private GameObject powerUpPrefab;
    [SerializeField] private float minPowerUpDelay;
    [SerializeField] private float maxPowerUpDelay;
    private float powerUpDelay = float.MaxValue;

    [SerializeField] private List<ShelfBehavior> destroyables;
    private BoolGrid grid;

    public GameObject inMatchBarriers, outOfMatchBarriers;

    public TextMeshProUGUI victoryText;
    public GameObject victoryParent;

    [SerializeField] private GameObject controlsParent;

    [SerializeField] private TextMeshPro teamDirtyPlayerCount;
    [SerializeField] private TextMeshPro teamCleanPlayerCount;

    [SerializeField] private RectTransform score, dirtyScore;
    [SerializeField] private GameObject scoreParent;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        
        isMatchRunning = false;
        matchStart = 0;
        

        inMatchBarriers.SetActive(false);
        outOfMatchBarriers.SetActive(true);
    }

    private void Start()
    {
        grid = FindObjectOfType<BoolGrid>();
        victoryParent.SetActive(false);
        SynchronizePlayers();
        
        if(isClient)
        {
            GetMatchRunning();
        }
    }
    

    private void SynchronizePlayers()
    {
        if (isServer)
        {
            return;
        }

        SynchronizePlayersCommand();
    }

    [Command(requiresAuthority = false)]
    private void SynchronizePlayersCommand()
    {
        foreach (var dirtPlayer in teamDirty)
        {
            SetTeam(dirtPlayer, true, false);
        }

        foreach (var cleanPlayer in teamClean)
        {
            SetTeam(cleanPlayer, false, false);
        }

        foreach (var lobbyPlayer in lobbyPlayers)
        {
            UnsetTeam(lobbyPlayer, false);
        }
    }

    [Command(requiresAuthority = false)]
    public void AddPlayer(Player player)
    {
        lobbyPlayers.Add(player);
        UnsetTeam(player, true);
        UpdatePlayerCountLabels(teamDirty.Count, teamClean.Count);
        //con.identity.gameObject.SetActive(false);
    }

    public void RemovePlayer(Player player)
    {
        lobbyPlayers.Remove(player);
        teamDirty.Remove(player);
        teamClean.Remove(player);
        UpdatePlayerCountLabels(teamDirty.Count, teamClean.Count);
    }

    [Command(requiresAuthority = false)]
    public void SetTeamCommand(Player player, bool isTeamDirty, bool respawn)
    {
        SetTeam(player, isTeamDirty, respawn);
    }

    public void SetTeam(Player player, bool isTeamDirty, bool respawn)
    {
        if (!isServer)
        {
            return;
        }

        Player.Team team;
        if (isTeamDirty)
        {
            if (respawn)
                player.Spawn(dirtSpawn.transform);
            team = Player.Team.Dirt;
            if (!teamDirty.Contains(player))
                teamDirty.Add(player);
            teamClean.Remove(player);
        }
        else
        {
            if (respawn)
                player.Spawn(cleanSpawn.transform);
            team = Player.Team.Clean;
            if (!teamClean.Contains(player))
                teamClean.Add(player);
            teamDirty.Remove(player);
        }

        if (countDownToMatchStart > 0)
        {
            player.isReady = true;
        }

        player.team = team;
        player.SetTeam(team);
        lobbyPlayers.Remove(player);
        UpdatePlayerCountLabels(teamDirty.Count, teamClean.Count);
    }

    [Command(requiresAuthority = false)]
    public void UnsetTeamCommand(Player player, bool respawn)
    {
        //player.ResetRpc();
        UnsetTeam(player, respawn);
    }

    public void UnsetTeam(Player player, bool respawn)
    {
        if (!isServer)
        {
            return;
        }

        if (respawn)
            player.Spawn(lobbySpawn.transform);
        player.team = Player.Team.Lobby;
        player.SetTeam(Player.Team.Lobby);
        player.isReady = false;
        if (!lobbyPlayers.Contains(player))
            lobbyPlayers.Add(player);
        teamDirty.Remove(player);
        teamClean.Remove(player);
        UpdatePlayerCountLabels(teamDirty.Count, teamClean.Count);
    }

    [ClientRpc]
    private void UpdatePlayerCountLabels(int teamDirtyCount, int teamCleanCount)
    {
        teamDirtyPlayerCount.SetText("Players: " + teamDirtyCount);
        teamCleanPlayerCount.SetText("Players: " + teamCleanCount);
    }

    private void FixedUpdate()
    {
        if (isServer)
        {
            if (isMatchRunning)
            {
                return;
            }

            if (!TeamsFullAndReady())
            {
                countDownToMatchStart = -1;
                return;
            }

            if (countDownToMatchStart > 0)
            {
                return;
            }

            StartCoroutine(BeginMatchCoroutine());
        }
    }

    private void Update()
    {
        if (isServer)
        {
            if (isMatchRunning && NetworkTime.time > (matchStart + matchDuration))
            {
                isMatchRunning = false;
                var bothTeams = teamDirty.Union(teamClean).ToList();
                foreach (var player in bothTeams)
                {
                    UnsetTeam(player, true);
                    player.ResetRpc();
                }

                float finalSCore = grid.GetDirtPercentage();

                grid.ResetField();
                foreach (var destroyable in destroyables)
                {
                    destroyable.DeleteContentsLocal();
                    destroyable.DeleteContents();
                }

                StopMatch(finalSCore);
            }
        }

        timer.gameObject.SetActive(isMatchRunning || countDownToMatchStart > 0);
        int time = countDownToMatchStart > 0
            ? (int) countDownToMatchStart
            : (int) (matchStart + matchDuration - NetworkTime.time);
        int seconds = time % 60;
        int minutes = time / 60;
        string timerText = ((minutes < 10) ? "0" + minutes : minutes.ToString()) + ":" +
                           ((seconds < 10) ? "0" + seconds : seconds.ToString());
        timer.text = countDownToMatchStart > 0 ? "Game starting in... " + timerText : timerText;

        timer.color = normalTimerColor;
        if (time <= 10)
            timer.color = almostOverTimerColor;

        powerUpDelay -= Time.deltaTime;
        if (powerUpDelay < 0)
        {
            SpawnPowerUp();
            powerUpDelay = float.MaxValue;
        }

        UpdateScoreUI();
    }

    private IEnumerator BeginMatchCoroutine()
    {
        countDownToMatchStart = countDownDuration;
        while (countDownToMatchStart > 0)
        {
            yield return null;
            countDownToMatchStart -= Time.deltaTime;
        }

        if (TeamsFullAndReady())
        {
            BeginMatch();
        }
    }

    private void BeginMatch()
    {
        matchStart = NetworkTime.time;
        isMatchRunning = true;
        PreparePowerUpSpawn();
        BeginMatch(NetworkTime.time);

        foreach (var p in teamClean)
        {
            p.isReady = false;
        }

        foreach (var p in teamDirty)
        {
            p.isReady = false;
        }
    }

    private bool TeamsFullAndReady()
    {
        if (teamClean.Count < teamCleanMinSize || teamDirty.Count < teamDirtMinSize)
            return false;

        foreach (Player player in teamDirty)
        {
            if (!player.isReady)
                return false;
        }

        foreach (Player player in teamClean)
        {
            if (!player.isReady)
                return false;
        }

        return true;
    }

    [ClientRpc]
    public void BeginMatch(double start)
    {

        matchStart = start;
        isMatchRunning = true;

        inMatchBarriers.SetActive(true);
        outOfMatchBarriers.SetActive(false);
        victoryParent.SetActive(false);
        controlsParent.SetActive(false);
    }

    [ClientRpc]
    public void StopMatch(float finalScoreDirty)
    {

        isMatchRunning = false;

        inMatchBarriers.SetActive(false);
        outOfMatchBarriers.SetActive(true);
        controlsParent.SetActive(true);


        grid.ResetField();

        if (finalScoreDirty > 0.5f)
            SetVictoryMessage(Player.Team.Dirt, finalScoreDirty);
        else
            SetVictoryMessage(Player.Team.Clean, 1.0f - finalScoreDirty);
    }

    public void SetVictoryMessage(Player.Team winner, float percent)
    {
        victoryParent.SetActive(true);
        int victoryPercent = (int) (percent * 100.0f);
        victoryText.text = "TEAM " + ((winner == Player.Team.Dirt) ? "DIRT" : "CLEAN") + " WON\n\nWITH " +
                           victoryPercent + "% COVERAGE";
    }

    private float GetDestroyableDestroyedPercentage()
    {
        int totalCount = destroyables.Count;
        int destroyedCount = 0;
        foreach (var destroyable in destroyables)
        {
            if (destroyable.dirty)
            {
                destroyedCount++;
                Debug.Log("Found a dirty");
            }
        }

        return ((float) destroyedCount) / totalCount;
    }

    private float GetTeamDirtyScore()
    {
        float destroyablesScore = GetDestroyableDestroyedPercentage();
        float gridScore = grid.GetDirtyPercentage();
        return (destroyablesScore + gridScore) / 2;
    }

    public void PreparePowerUpSpawn()
    {
        powerUpDelay = UnityEngine.Random.Range(minPowerUpDelay, maxPowerUpDelay);
        Debug.Log("Spawn Power Up in: " + powerUpDelay);
    }

    private void SpawnPowerUp()
    {
        if (!isServer)
        {
            return;
        }

        Debug.Log("Spawn Power Up");
        int spawnIndex = (int) (UnityEngine.Random.value * powerUpSpawnPoints.childCount);
        spawnIndex = spawnIndex >= powerUpSpawnPoints.childCount ? powerUpSpawnPoints.childCount - 1 : spawnIndex;
        Vector3 location = powerUpSpawnPoints.GetChild(spawnIndex).position;
        var powerUp = Instantiate(powerUpPrefab);
        powerUp.transform.position = location;
        NetworkServer.Spawn(powerUp);
    }

    private void UpdateScoreUI()
    {
        if (!instance.isMatchRunning)
        {
            scoreParent.gameObject.SetActive(false);
            return;
        }

        scoreParent.gameObject.SetActive(true);
        float dirtPercentage = grid.GetDirtPercentage();
        dirtyScore.sizeDelta = new Vector2(dirtPercentage * score.sizeDelta.x, dirtyScore.sizeDelta.y);
        dirtyScore.anchoredPosition = new Vector2(dirtyScore.sizeDelta.x * 0.5f, dirtyScore.anchoredPosition.y);
    }

    [Command(requiresAuthority = false)]
    private void GetMatchRunning()
    {
        SetMatchRunning(isMatchRunning, matchStart);
    }

    [ClientRpc]
    private void SetMatchRunning(bool isRunning, double start)
    {
        isMatchRunning = isRunning;
        matchStart = start;
        
        inMatchBarriers.SetActive(isRunning);
        outOfMatchBarriers.SetActive(!isRunning);
    }
}