using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour{
    Vector3 oldPos = new Vector3(0,0,0);

    [SerializeField]
    float threshold = 0.05f;

    Animator animator;
    Animator Animator
    {
        get
        {
            if(animator == null)
            {
                animator = GetComponent<Animator>();
            }
            return animator;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 newPos = transform.position;
        
        if ((newPos - oldPos).magnitude > threshold)
        {
            Animator.SetBool("isWalking", true);
        }
        else Animator.SetBool("isWalking", false);
        
        oldPos = newPos;
    }

    public void SetWalkingSpeed(float walkingSpeed)
    {
        Animator.SetFloat("walkingSpeed", walkingSpeed);
    }
}
