using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;

namespace Clirty
{
    public class Grid : MonoBehaviour
    {
        public struct Int2
        {
            public int x;
            public int y;

            public Int2(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }

        private int teamCleanPoints;
        private int teamDirtPoints;
        [SerializeField] private uint width;
        [SerializeField] private uint height;

        [SerializeField] private GameObject tilePrefab;

        private GridTile[,] grid;

        private float tileWidth;
        private float tileHeight;

        private float xStart;
        private float yStart;

        private float leftBoundary;
        private float rightBoundary;
        private float upperBoundary;
        private float lowerBoundary;

        int endlessLoopPreventer = 0;

        private List<Player> players = new List<Player>();

        public int getTeamCleanPoints()
        {
            return teamCleanPoints;
        }

        public int getTeamDirtPoints()
        {
            return teamDirtPoints;
        }

        public void addTeamCleanPoints(int amount)
        {
            teamCleanPoints += amount;
        }

        public void addTeamDirtPoints(int amount)
        {
            teamDirtPoints += amount;
        }

        public void AddPlayer(Player newPlayer)
        {
            players.Add(newPlayer);
        }

        // Start is called before the first frame update
        void Start()
        {
            teamCleanPoints = 0;
            teamDirtPoints = 0;

            grid = new GridTile[width, height];
            tileWidth = tilePrefab.transform.localScale.x * 10;
            float tileHeight = tilePrefab.transform.localScale.z * 10;
            xStart = 0 - width / 2.0f * tileWidth + tileWidth / 2.0f;
            yStart = 0 - height / 2.0f * tileHeight + tileHeight / 2.0f;
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    grid[x, y] = Instantiate(tilePrefab).GetComponent<GridTile>();
                    grid[x, y].transform.parent = transform;
                    Vector3 position = new Vector3(xStart + x * tileWidth, 0.1f, yStart + y * tileHeight);
                    grid[x, y].transform.localPosition = position;
                }
            }
        }


        // Update is called once per frame
        void Update()
        {
        }
        void OnApplicationQuit()
        {
            //addTeamCleanPoints(20);
            countPoints();
            Debug.Log("Team Clean points: " + teamCleanPoints);
            Debug.Log("Team Dirt points: " + teamDirtPoints);
        }
        //Calculates the points once by going through the children of the grid and checks if they're dirty or not
        public void countPoints()
        {
            foreach (Transform child in transform)
            {
                if (child.GetComponent<GridTile>().getDirty())
                    teamDirtPoints += 10;

                if (!child.GetComponent<GridTile>().getDirty())
                    teamCleanPoints += 10;
            }
        }

        /*private void InitializePlayers()
        {
            var playerObjects = GameObject.FindGameObjectsWithTag("Player");
            players = new Player[playerObjects.Length];
            for (int i = 0; i < playerObjects.Length; i++)
            {
                players[i] = playerObjects[i].GetComponent<Player>();
            }
        }*/

        void LateUpdate()
        {
            /*if (players == null)
            {
                InitializePlayers();
            }*/

            foreach (var player in players)
            {
                var playerPosition = new Vector2(player.transform.position.x, player.transform.position.z);
                ChangeTileAction action = NoAction;
                if (player is CleanPlayer)
                {
                    action = MakeTileClean;
                }
                else if (player is DirtPlayer)
                {
                    action = MakeTileDirty;
                }
                ChangeTile(playerPosition, action);
            }
        }

        public void ChangeCone(Vector2 start, Vector2 direction, float range, float angle, ChangeTileAction action)
        {
            float halfAngle = angle * 0.5f;
            float tanHalfAngle = Mathf.Tan(halfAngle);
            direction = direction.normalized;
            Vector2 perpendicular = new Vector2(direction.y, -direction.x);

            start = ClampPositionToBoundary(start, direction);
            Vector2 position = start;
            float distance = (position - start).magnitude;
            while (distance < range && !OutOfBounds(position) && endlessLoopPreventer++ < 1000000)
            {
                float halfWidth = distance * tanHalfAngle;

                ChangeLine(position - perpendicular * halfWidth, position + perpendicular * halfWidth, action);
                var index = GetIndex(position);
                if (OutOfBounds(index))
                {
                    break;
                }
                var boundaries = grid[index.x, index.y].GetBoundaries();
                position = IntersectWithBoundary(position, direction, boundaries);
                distance = (position - start).magnitude;
            }
        }

        private Vector2 ClampPositionToBoundary(Vector2 position, Vector2 direction)
        {
            if (!OutOfBounds(position))
            {
                return position;
            }
            float step = 0;
            if (position.x < leftBoundary)
            {
                step = (leftBoundary - position.x) / direction.x;
            }
            else if (position.x >= rightBoundary)
            {
                step = (rightBoundary - position.x) / direction.x;
            }
            if (position.y < upperBoundary)
            {
                float newStep = (upperBoundary - position.y) / direction.y;
                step = newStep > step ? newStep : step;
            }
            else if (position.y >= lowerBoundary)
            {
                float newStep = (lowerBoundary - position.y) / direction.y;
                step = newStep > step ? newStep : step;
            }
            if (step > 0)
            {
                position += direction * (step + 0.001f);
            }
            return position;
        }

        public void ChangeLine(Vector2 start, Vector2 end, ChangeTileAction action)
        {
            Vector2 direction = end - start;
            direction = direction.normalized;

            start = ClampPositionToBoundary(start, direction);
            end = ClampPositionToBoundary(end, -direction);

            if (OutOfBounds(start) || OutOfBounds(end))
            {
                return;
            }

            Vector2 position = start;
            float distance = (end - start).magnitude;
            while ((position - start).magnitude < distance && endlessLoopPreventer++ < 1000000)
            {
                Int2 index = GetIndex(position);
                if (OutOfBounds(index))
                {
                    break;
                }
                action(index);
                var boundaries = grid[index.x, index.y].GetBoundaries();
                position = IntersectWithBoundary(position, direction, boundaries);
            }
        }

        private Vector2 IntersectWithBoundary(Vector2 position, Vector2 direction, Vector4 boundaries)
        {
            float dx = direction.x > 0 ? boundaries.y - position.x : position.x - boundaries.x;
            float dy = direction.y > 0 ? boundaries.w - position.y : position.y - boundaries.z;
            float step = Mathf.Min(dx / direction.x, dy / direction.y);
            position += direction * (step + 0.001f);
            return position;
        }

        private Int2 GetIndex(Vector2 position)
        {
            return new Int2(Mathf.RoundToInt((position.x - leftBoundary) / tileWidth), Mathf.RoundToInt((position.y - upperBoundary) / tileHeight));
        }

        private bool OutOfBounds(Int2 index)
        {
            return index.x < 0 || index.y < 0 || index.x >= width || index.y >= height;
        }

        private bool OutOfBounds(Vector2 position)
        {
            return position.x < leftBoundary || position.y < upperBoundary || position.x >= rightBoundary || position.y >= lowerBoundary;
        }

        private void ChangeTilesCross(Vector2 center, float dx, float dy, ChangeTileAction action)
        {
            ChangeTile(new Vector2(center.x - dx, center.y - dy), action);
            ChangeTile(new Vector2(center.x - dx, center.y + dy), action);
            ChangeTile(new Vector2(center.x + dx, center.y - dy), action);
            ChangeTile(new Vector2(center.x + dx, center.y + dy), action);
        }

        public void ChangeCircle(Vector2 center, float radius, ChangeTileAction action)
        {
            if (!CircleInsideGrid(center, radius))
            {
                return;
            }
            Debug.Log("Draw circle");
            Vector2 position = center;
            position.y += radius;
            float lowerBound = center.y - radius;
            float radiusSq = radius * radius;

            float dy = 0;
            while (dy < radius && endlessLoopPreventer++ < 1000000)
            {
                float dySq = dy * dy;
                float halfWidth = Mathf.Sqrt(radiusSq - dySq);
                float dx = 0;
                while (dx < halfWidth && endlessLoopPreventer++ < 1000000)
                {
                    ChangeTilesCross(center, dx, dy, action);
                    dx += tileWidth;
                }
                dy += tileHeight;
            }
        }

        private bool CircleInsideGrid(Vector2 center, float radius)
        {
            return center.x + radius >= leftBoundary && center.x - radius <= rightBoundary && center.y + radius >= upperBoundary && center.y - radius <= lowerBoundary;
        }

        public delegate void ChangeTileAction(Int2 position);

        private void ChangeTile(Vector2 position, ChangeTileAction action)
        {
            Int2 index = GetIndex(position);
            if (OutOfBounds(index))
            {
                return;
            }
            action(index);
        }

        public void MakeTileDirty(Int2 index)
        {
            if (OutOfBounds(index))
            {
                return;
            }
            grid[(int)index.x, (int)index.y].MakeDirty();
        }

        public void MakeTileClean(Int2 index)
        {
            if (OutOfBounds(index))
            {
                return;
            }
            grid[(int)index.x, (int)index.y].MakeClean();
        }

        private void NoAction(Int2 position)
        {
            throw new Exception("This should not happen.");
        }
    }
}