using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Clirty
{
    public class DirtPlayer : Player
    {
        [SerializeField] private GameObject grenadePref;
        // Start is called before the first frame update
        void Start()
        {
            ThrowGrenade();
        }

        private void ThrowGrenade()
        {
            var grenade = Instantiate(grenadePref);
            grenade.transform.parent = transform;
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
