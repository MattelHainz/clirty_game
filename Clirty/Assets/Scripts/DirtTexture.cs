using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Clirty {
    public class DirtTexture
    {
        private Texture2D texture;
        private bool textureChanged = false;

        public Texture2D GetTexture()
        {
            return texture;
        }

        public DirtTexture (bool[,] grid)
        {
            var width = grid.GetLength(0);
            var height = grid.GetLength(1);
            texture = new Texture2D(width, height, TextureFormat.RFloat, true);
            texture.anisoLevel = 2;

            for(int x = 0; x < width; x++)
            {
                for(int y = 0; y < height; y++)
                {
                    texture.SetPixel(x, y, Color.black);
                }
            }
            texture.Apply();
            //TextureScaler.scale(texture, 1024, 1024);
            float[][] kernel = new float[][]
            {
                new float[] {1, 1, 1},
                new float[] {1, 1, 1},
                new float[] {1, 1, 1},
            };
            //texture = TextureFilter.Convolution(texture, kernel, 1);
            //texture = TextureFilter.Convolution(texture, TextureFilter.GAUSSIAN_KERNEL_3, 1);
            //texture = TextureFilter.Convolution(texture, new float[][] { new float[] { 1.0f / 9 } }, 1);
            //texture = TextureFilter.Convolution(texture, TextureFilter.SHARPEN_KERNEL, 1);
            //texture = TextureFilter.Grayscale(texture);
        }

        public void SetPixel(int x, int y, bool dirty)
        {
            texture.SetPixel(x, y, dirty ? Color.white : Color.black);
            textureChanged = true;
        }

        public void UpdateTexture()
        {
            if (textureChanged)
            {
                texture.Apply();
                textureChanged = false;
            }
        }
    }
}
