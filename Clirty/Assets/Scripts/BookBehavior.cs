using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookBehavior : MonoBehaviour
{
    [SerializeField] GameObject beam;

    public GameObject beamIns;
    Rigidbody rigidBody;
    bool rotate = false;
    GameObject rotationCenter;
    public GameObject source;
    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (rotate)
        {
            transform.position = rotationCenter.transform.position + (transform.position - rotationCenter.transform.position).normalized;
            transform.RotateAround(rotationCenter.transform.position, Vector3.up, 90f * Time.deltaTime);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Floor"))
        {
            if(ClirtyNetworkManager.ClirtySingleton.player != null && ClirtyNetworkManager.ClirtySingleton.player.team == Clirty.Player.Team.Clean)
            {
                if (beamIns != null)
                {
                    Destroy(beamIns);
                }
                beamIns = Instantiate(beam, transform.position + new Vector3(0, 0.75f, 0), new Quaternion());
            }
        }
    }

    public void Collected(GameObject player)
    {
        //deleting beam
        if (beamIns != null)
            Destroy(beamIns);

        this.transform.SetParent(player.gameObject.transform);
        this.transform.localScale = new Vector3(transform.lossyScale.x / 2, transform.lossyScale.y / 2, transform.lossyScale.z / 2);
        rigidBody.useGravity = false;
        rigidBody.isKinematic = true;
        transform.position = player.transform.position + new Vector3(0, 0.5f, 1);
        rotationCenter = player;
        rotate = true;
    }

    public void DespawnBeam()
    {
        if(beamIns != null)
        {
            Destroy(beamIns);
        }
    }
}
