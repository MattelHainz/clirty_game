using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridTile : MonoBehaviour
{
    private bool dirty;
    private bool dirtied;
    private bool cleaned;
    private float width;
    private float height;

    public bool getDirty()
    {
        return dirty;
    }

    public void Start()
    {
        width = transform.localScale.x;
        height = transform.localScale.z;
    }
    public void Update()
    {
        dirtied = false;
        cleaned = false;
    }
    public void MakeDirty()
    {
        dirtied = true;
        if (dirty)
        {
            return;
        }

        dirty = true;
        gameObject.GetComponent<MeshRenderer>().material.SetColor("_BaseColor", Color.red);
        Debug.Log("Dirtied.");
        if (cleaned)
        {
            Debug.Log("Undefined behaviour.");
        }
    }

    public void MakeClean()
    {
        cleaned = true;
        if (!dirty)
        {
            return;
        }

        dirty = false;
        gameObject.GetComponent<MeshRenderer>().sharedMaterial.SetColor("_BaseColor", Color.white);
        Debug.Log("Cleaned");
        if (dirtied)
        {
            Debug.Log("Undefined behaviour.");
        }
    }

    public Vector4 GetBoundaries()
    {
        float x0 = transform.position.x - width * 0.5f;
        float x1 = transform.position.x + width * 0.5f;
        float y0 = transform.position.z - height * 0.5f;
        float y1 = transform.position.z + height * 0.5f;
        return new Vector4(x0, x1, y0, y1);
    }
}
