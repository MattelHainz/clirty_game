using System;
using System.Collections;
using System.Collections.Generic;
using Clirty;
using Mirror;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

// Clirty Version of Basic Networkmanager. Use this to override callbacks.
public class ClirtyNetworkManager : NetworkManager
{
    public static ClirtyNetworkManager ClirtySingleton => (ClirtyNetworkManager)singleton;
    private PlayerInput input;
    public static bool lastConnectionLost = false;
    public string localPlayerName;
    public Player player;

    [SerializeField] private bool startAsServerInEditor;
        
    public override void Awake()
    {
            base.Awake();
            input = GetComponent<PlayerInput>();
                
            #if UNITY_EDITOR
            if(startAsServerInEditor) StartServer();
            #endif
    }
        
    public override void OnClientConnect(NetworkConnection conn)
    {
            lastConnectionLost = false;
            base.OnClientConnect(conn);
    }
        
    public override void OnServerConnect(NetworkConnection conn)
    {
            base.OnServerConnect(conn);
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
            try
            {
                MatchManager.instance.RemovePlayer(conn.identity.GetComponent<Player>());
            }
            catch (Exception e) { }
            base.OnServerDisconnect(conn);
    }


    private void OnConnectedToServer()
    {
            //SceneManager.LoadScene(0);
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
            lastConnectionLost = true;
            base.OnClientDisconnect(conn);
    }

    public void AttachToInput(string actionName, Action<InputAction.CallbackContext> action)
    {
            input.currentActionMap.FindAction(actionName).performed += action;
    }

        public void AttachToInputStarted(string actionName, Action<InputAction.CallbackContext> action)
        {
            input.currentActionMap.FindAction(actionName).started += action;
        }

        public void AttachToInputCanceled(string actionName, Action<InputAction.CallbackContext> action)
        {
            input.currentActionMap.FindAction(actionName).canceled += action;
        }

    public PlayerInput getPlayerInput()
    {
            return input;
    }
        
        
        
        
        
}
