using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace Clirty
{
    [RequireComponent(typeof(CharacterController))]
    public class Player : NetworkBehaviour
    {
        public enum Team
        {
            Lobby,
            Dirt,
            Clean
        }

        [SerializeField] public Team team;
        [SerializeField] private float standardSpeed;
        [SerializeField] private float powerUpSpeed;
        private float playerSpeed;
        [SerializeField] public float powerUpScale;

        // Items
        [SerializeField] private GameObject grenadePrefab;
        [SerializeField] private GameObject waterHosePrefab;
        [SerializeField] private float itemCooldown;
        [SerializeField] private GameObject grenadeModel;
        [SerializeField] private GameObject waterHoseModel;

        [SerializeField] private Transform physicalPlayer;

        [SerializeField] private GameObject teamCleanModel;
        [SerializeField] private GameObject teamDirtyModel;
        [SerializeField] private GameObject lobbyModel;

        // Combat
        [SerializeField] private int maxAmmo;
        [SyncVar] private int currentAmmo;
        [SerializeField] private float ammoRechargeTime;
        [SerializeField] private float ammoRechargeDeadTime;
        private float ammoRechargeTimeElapsed;
        private float ammoRechargeDeadTimeElapsed;
        private bool tryingToUseItem;

        // Team clean passive action
        [SerializeField] private float passiveCleaningWidth;
        private float passiveCleaningWidthMultiplier = 1;
        private Vector2 previousPosition;
        private Vector2 previousRight;

        // Team dirt passive action
        [SerializeField] private float passiveDirtyingRadius;
        [SerializeField] private float passiveDirtyingRadiusVariation;
        [SerializeField] private float passiveDirtyingTimeInterval;
        private float timeSinceLastPassiveDirt = 0;
        LineRenderer grenadeIndicatorLine;
        private bool drawGrenadeCurve;
        private float grenadeThrowStartTime;
        private Grenade grenade;

        private BoolGrid grid;
        private CharacterController controller;
        private Vector3 playerVelocity;
        private Vector2 inputDir;
        private GameObject[] destroyables;
        private PlayerInterface playerInterface;
        [SyncVar] private float stunnedForSeconds;
        [SerializeField] private float invincibilityAfterStun;
        [SyncVar] private float invincibleForSeconds;
        private bool wasInvincible = true;
        private bool isBlinking;

        [SyncVar] public bool isReady = false;
        [SyncVar] public string playerName;

        // Destroyables
        [SerializeField] private float timeToDestroy;
        [SerializeField] private GameObject destroyUI;
        [SerializeField] private GameObject destroyProgressParent;
        [SerializeField] private RectTransform destroyProgress;
        [SerializeField] private GameObject notAllItemsCross;
        [SerializeField] private float objectDestroyDirtRadius;
        [SerializeField] private float objectRepairCleanWidth;
        [SerializeField] private GameObject objectDestroyedParticleEffect;
        private float destroyStartTime;
        private bool isDestroying;
        private string[] miniGameButtons;
        private bool[] miniGamePressed = new bool[] {false, false, false, false};
        private bool miniGame = false;
        [SyncVar] private float powerUpForSeconds;


        PlayerAnimation cleanPlayerAnimation;
        PlayerAnimation dirtyPlayerAnimation;


        private GameObject destroyable;

        public List<BookBehavior> inventory = new List<BookBehavior>();

        public Canvas canvas;

        [SerializeField] AmmoUI ammoUI;

        //private NavMeshAgent agent;
        //private bool useNav = false;
        public GameObject stunDisplay;

        private Material material;
        [SerializeField] private Material dirtyMaterial;
        [SerializeField] private Material cleanMaterial;
        [SerializeField] private Material lobbyMaterial;

        AudioSource m_MyAudioSource;


        private void Start()
        {
            m_MyAudioSource = GetComponent<AudioSource>();
            grid = FindObjectOfType<BoolGrid>();
            controller = gameObject.GetComponent<CharacterController>();
            playerInterface = GetComponent<PlayerInterface>();
            playerInterface.SetMiniGameVisible(false);
            //agent = GetComponent<NavMeshAgent>();
            destroyables = GameObject.FindGameObjectsWithTag("Destroyable");

            teamCleanModel.SetActive(false);
            cleanPlayerAnimation = teamCleanModel.GetComponent<PlayerAnimation>();
            teamDirtyModel.SetActive(false);
            dirtyPlayerAnimation = teamDirtyModel.GetComponent<PlayerAnimation>();
            lobbyModel.SetActive(true);

            PlayerInput pi = ClirtyNetworkManager.ClirtySingleton.getPlayerInput();
            miniGameButtons = new string[]
            {
                pi.currentActionMap.FindAction("MiniGame0").controls[0].name,
                pi.currentActionMap.FindAction("MiniGame1").controls[0].name,
                pi.currentActionMap.FindAction("MiniGame2").controls[0].name,
                pi.currentActionMap.FindAction("MiniGame3").controls[0].name
            };


            if (hasAuthority)
            {
                ClirtyNetworkManager.ClirtySingleton.AttachToInput("Movement", OnMove);
                ClirtyNetworkManager.ClirtySingleton.AttachToInput("Ready", OnReadyButton);
                ClirtyNetworkManager.ClirtySingleton.AttachToInputStarted("Item", OnItemStarted);
                ClirtyNetworkManager.ClirtySingleton.AttachToInputCanceled("Item", OnItemCanceled);
                //ClirtyNetworkManager.ClirtySingleton.AttachToInput("Destroy", OnDestroyClick);
                ClirtyNetworkManager.ClirtySingleton.AttachToInputStarted("Destroy", OnDestroyStarted);
                ClirtyNetworkManager.ClirtySingleton.AttachToInputCanceled("Destroy", OnDestroyCanceled);
                ClirtyNetworkManager.ClirtySingleton.AttachToInput("Repair", OnRepairClick);
                ClirtyNetworkManager.ClirtySingleton.AttachToInput("MiniGame0", OnMiniGameClick);
                ClirtyNetworkManager.ClirtySingleton.AttachToInput("MiniGame1", OnMiniGameClick);
                ClirtyNetworkManager.ClirtySingleton.AttachToInput("MiniGame2", OnMiniGameClick);
                ClirtyNetworkManager.ClirtySingleton.AttachToInput("MiniGame3", OnMiniGameClick);
                ClirtyNetworkManager.ClirtySingleton.player = this;

                SetName(ClirtyNetworkManager.ClirtySingleton.localPlayerName);
                previousPosition = new Vector2(transform.position.x, transform.position.z);
                previousRight = new Vector2(transform.right.x, transform.right.z);
                //Camera.main.transform.SetParent(transform);

                currentAmmo = maxAmmo;
                ammoUI.transform.parent.gameObject.SetActive(true);
                ammoUI.SetMaxAmmo(maxAmmo);
                ammoUI.SetTeam(team);

                grenadeIndicatorLine = GetComponent<LineRenderer>();
            }
        }

        void Update()
        {
            playerInterface.SetName(playerName);
            playerInterface.SetReady(isReady);

            //Server Update
            if (isServer)
            {
                ServerUpdate();
            }

            //Power Up Update
            if (powerUpForSeconds > 0)
            {
                stunnedForSeconds = 0;
                transform.localScale = Vector3.one * powerUpScale;
                playerSpeed = powerUpSpeed;
            }
            else
            {
                transform.localScale = Vector3.one;
                playerSpeed = standardSpeed;
            }

            dirtyPlayerAnimation.SetWalkingSpeed(playerSpeed / standardSpeed);
            cleanPlayerAnimation.SetWalkingSpeed(playerSpeed / standardSpeed);

            // Check for Stun
            if (stunnedForSeconds > 0)
            {
                stunDisplay.SetActive(true);
                return;
            }

            stunDisplay.SetActive(false);

            // Authority Update
            if (hasAuthority)
            {
                AuthorityPlayerUpdate();
            }
        }

        private void ServerUpdate()
        {
            powerUpForSeconds -= Time.deltaTime;
            stunnedForSeconds -= Time.deltaTime;
            invincibleForSeconds -= Time.deltaTime;
            if (stunnedForSeconds < 0 && !wasInvincible)
            {
                wasInvincible = true;
                invincibleForSeconds = invincibilityAfterStun;
                InvincibilityBlinkingRpc(invincibleForSeconds);
            }

            if (drawGrenadeCurve)
            {
                DrawGrenadeCurve(Time.time - grenadeThrowStartTime);
            }
        }

        private void AuthorityPlayerUpdate()
        {
            AmmoRecharge();
            ammoUI.SetCurrentAmmo(currentAmmo, ammoRechargeTimeElapsed / ammoRechargeTime);
            Vector3 move = new Vector3(inputDir.x, -10.0f, inputDir.y);
            if (!miniGame)
                controller.Move(move * Time.deltaTime * playerSpeed);
            PassiveColoringAction();
            if ((inputDir.x != 0 || inputDir.y != 0))
            {
                //transform.forward = new Vector3(inputDir.x, 0, inputDir.y);
                CommandSetRotation(inputDir);
            }

            if (drawGrenadeCurve)
            {
                DrawGrenadeCurve(Time.time - grenadeThrowStartTime);
            }

            if (tryingToUseItem)
            {
                OnItemStarted();
            }

            destroyable = GetDestroyableInRadius();

            if (isDestroying)
            {
                destroyProgressParent.SetActive(true);
                Destroy();
            }
            else
            {
                destroyProgressParent.SetActive(false);
            }
        }

        [ClientRpc]
        public void ResetRpc()
        {
            try
            {
                //StopAllCoroutines();
                currentAmmo = maxAmmo;
                tryingToUseItem = false;
                drawGrenadeCurve = false;
                if (grenade != null)
                    Destroy(grenade);
                stunnedForSeconds = 0;
                invincibleForSeconds = 0;
                powerUpForSeconds = 0;
                isBlinking = false;
                isReady = false;
                isDestroying = false;
                if (inventory != null)
                {
                    foreach (var item in inventory)
                    {
                        if (item != null)
                            Destroy(item);
                    }
                    

                    //inventory.Clear();
                    inventory = new List<BookBehavior>();
                }

                if (grenadeIndicatorLine != null)
                    grenadeIndicatorLine.positionCount = 0;
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
            
        }


        [Command]
        private void DebugLog(string text)
        {
            Debug.Log(text);
        }

        private void AmmoRecharge()
        {
            if (currentAmmo >= maxAmmo)
            {
                return;
            }

            ammoRechargeDeadTimeElapsed += Time.deltaTime;
            if (ammoRechargeDeadTimeElapsed < ammoRechargeDeadTime)
            {
                return;
            }

            ammoRechargeTimeElapsed += Time.deltaTime;
            if (ammoRechargeTimeElapsed < ammoRechargeTime)
            {
                return;
            }

            currentAmmo++;
            ammoRechargeDeadTimeElapsed = ammoRechargeTimeElapsed = 0;
        }

        [Command(requiresAuthority = false)]
        public void CommandSetRotation(Vector2 input)
        {
            physicalPlayer.LookAt(transform.position + new Vector3(input.x, 0, input.y));
            RPCSetRotation(input);
        }

        [ClientRpc]
        public void RPCSetRotation(Vector2 input)
        {
            physicalPlayer.LookAt(transform.position + new Vector3(input.x, 0, input.y));

            if (destroyable != null)
            {
            }
        }

        private void LateUpdate()
        {
            previousPosition = new Vector2(transform.position.x, transform.position.z);
            previousRight = new Vector2(physicalPlayer.right.x, physicalPlayer.right.z).normalized;
        }

        private void PassiveColoringAction()
        {
            if (powerUpForSeconds < 0)
            {
                return;
            }

            if (team == Team.Dirt)
            {
                PassiveDirtyingAction();
            }
            else if (team == Team.Clean)
            {
                PassiveCleaningAction();
            }
        }

        private void PassiveDirtyingAction()
        {
            timeSinceLastPassiveDirt += Time.deltaTime;
            if (timeSinceLastPassiveDirt > passiveDirtyingTimeInterval)
            {
                timeSinceLastPassiveDirt = 0;
                float radius = passiveDirtyingRadius * UnityEngine.Random.Range(1 - passiveDirtyingRadiusVariation,
                    1 + passiveDirtyingRadiusVariation);
                PassiveDirtyingActionCommand(new Vector2(transform.position.x, transform.position.z), radius);
            }
        }

        [Command]
        private void PassiveDirtyingActionCommand(Vector2 center, float radius)
        {
            grid.DrawCircleServer(center, radius, true);
        }

        private void PassiveCleaningAction()
        {
            Vector2 position = new Vector2(transform.position.x, transform.position.z);
            Vector2 right = new Vector2(physicalPlayer.right.x, physicalPlayer.right.z).normalized *
                            passiveCleaningWidth * passiveCleaningWidthMultiplier * 0.5f;

            PassiveCleaningActionCommand(previousPosition - previousRight, previousPosition + previousRight,
                position + right, position - right);

            previousPosition = position;
            previousRight = right;
        }

        [Command]
        private void PassiveCleaningActionCommand(Vector2 a, Vector2 b, Vector2 c, Vector2 d)
        {
            grid.DrawSquareServer(a, b, c, d, false);
        }

        private GameObject GetDestroyableInRadius()
        {
            foreach (GameObject go in destroyables)
            {
                if (Vector3.Distance(transform.position, go.transform.position) < 3)
                {
                    ShowDestroyUI();
                    return go;
                }
            }

            destroyUI.SetActive(false);
            return null;
        }

        private void ShowDestroyUI()
        {
            destroyUI.SetActive(false);
            notAllItemsCross.SetActive(false);
            if (destroyable == null || stunnedForSeconds > 0)
            {
                return;
            }

            bool destroyableDirty;
            if (destroyable.TryGetComponent(out Destroyable destroyableScript))
            {
                destroyableDirty = destroyableScript.dirty;
            }
            else if (destroyable.TryGetComponent(out ShelfBehavior shelfBehavior))
            {
                destroyableDirty = shelfBehavior.dirty;
                if (team == Team.Clean && destroyableDirty && !shelfBehavior.PlayerHasAllObjects(this))
                {
                    notAllItemsCross.SetActive(true);
                }
            }
            else
            {
                return;
            }

            if (team == Team.Clean && destroyableDirty || team == Team.Dirt && !destroyableDirty)
            {
                destroyUI.SetActive(true);
            }
        }

        public void Stun(float time)
        {
            if (invincibleForSeconds > 0 || stunnedForSeconds > 0)
            {
                return;
            }

            wasInvincible = false;
            isDestroying = false;
            stunnedForSeconds = time;
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            inputDir = context.ReadValue<Vector2>();
        }

        public void OnReadyButton(InputAction.CallbackContext context)
        {
            SetReady(!isReady);
        }

        private void OnDestroyStarted(InputAction.CallbackContext context)
        {
            bool dirty = false;
            if (destroyable.TryGetComponent<Destroyable>(out var script))
            {
                dirty = script.dirty;
            }
            else if (destroyable.TryGetComponent<ShelfBehavior>(out var sScript))
            {
                dirty = sScript.dirty;
            }
        
            if (stunnedForSeconds > 0 || team != Team.Dirt || destroyable == null || dirty)
            {
                return;
            }

            isDestroying = true;
            destroyStartTime = Time.time;
        }

        private void Destroy()
        {
            if (team != Team.Dirt || destroyable == null)
            {
                return;
            }

            float timeElapsed = Time.time - destroyStartTime;
            destroyProgress.localScale = new Vector3(timeElapsed / timeToDestroy, destroyProgress.localScale.y,
                destroyProgress.localScale.z);

            if (timeElapsed < timeToDestroy)
            {
                return;
            }

            isDestroying = false;

            DestroyShelf();
            DestroyDestroyable();
            ObjectDestroyedCommand();
        }

        [Command]
        private void ObjectDestroyedCommand()
        {
            grid.DrawCircleServer(new Vector2(transform.position.x, transform.position.z), objectDestroyDirtRadius,
                true);
            ObjectDestroyedParticleEffectRpc();
        }

        [ClientRpc]
        private void ObjectDestroyedParticleEffectRpc()
        {
            objectDestroyedParticleEffect.transform.position = transform.position;
            Instantiate(objectDestroyedParticleEffect);
        }

        private void OnDestroyCanceled(InputAction.CallbackContext context)
        {
            isDestroying = false;
        }

        private void DestroyShelf()
        {
            var shelfScript = destroyable.GetComponent<ShelfBehavior>();
            if (shelfScript == null || shelfScript.dirty)
            {
                return;
            }

            shelfScript.MakeAllDirty();
        }

        private void DestroyDestroyable()
        {
            var destroyScript = destroyable.GetComponent<Destroyable>();
            if (destroyScript == null || destroyScript.dirty)
            {
                return;
            }

            destroyScript.MakeDirty();
        }

        public void OnRepairClick(InputAction.CallbackContext context)
        {
            if (context.performed && team == Team.Clean)
            {
                GetComponent<AudioScript>().playRepair();

                if (destroyable != null)
                {
                    var shelfScript = destroyable.GetComponent<ShelfBehavior>();
                    if (shelfScript != null)
                    {
                        if (shelfScript.dirty)
                        {
                            bool minigame = shelfScript.MakeClean(this);
                            if (!minigame)
                            {
                                ObjectRepairedCommand();
                            }
                        }
                    }

                    var destroyScript = destroyable.GetComponent<Destroyable>();
                    if (destroyScript != null)
                    {
                        if (destroyScript.dirty)
                        {
                            destroyScript.MakeClean();
                            ObjectRepairedCommand();
                        }
                    }
                }
            }
        }

        [Command]
        private void ObjectRepairedCommand()
        {
            Vector2 center = new Vector2(transform.position.x, transform.position.z);
            Vector2 a = center - new Vector2(0.5f * objectRepairCleanWidth, 0);
            Vector2 b = center - new Vector2(0, 0.5f * objectRepairCleanWidth);
            Vector2 c = center + new Vector2(0.5f * objectRepairCleanWidth, 0);
            Vector2 d = center + new Vector2(0, 0.5f * objectRepairCleanWidth);
            grid.DrawSquareServer(a, b, c, d, false, 0.3f);
        }

        public void OnMiniGameClick(InputAction.CallbackContext context)
        {
            if (!miniGame)
            {
                return;
            }

            int i = 0;
            while (i < 4)
            {
                if (context.control.name == miniGameButtons[i])
                {
                    break;
                }

                GetComponent<AudioScript>().playButtonClickMiniGame();
                i++;
            }

            bool beforesTrue = true;
            for (int j = 0; j < i; j++)
            {
                if (!miniGamePressed[j])
                {
                    beforesTrue = false;
                }
            }

            if (beforesTrue)
            {
                miniGamePressed[i] = true;
                playerInterface.SetMiniGameProgress(i + 1);
            }


            if (miniGamePressed[0] && miniGamePressed[1] && miniGamePressed[2] && miniGamePressed[3])
            {
                miniGamePressed = new bool[] {false, false, false, false};
                EndMiniGame();
            }
        }

        public void InitiateMiniGame()
        {
            playerInterface.SetMiniGameVisible(true);
            playerInterface.SetMiniGameProgress(0);

            ShuffleMiniGame();

            playerInterface.SetButtonText(miniGameButtons);

            miniGame = true;
        }

        private void EndMiniGame()
        {
            playerInterface.SetMiniGameVisible(false);

            miniGame = false;

            ObjectRepairedCommand();
        }


        private void ShuffleMiniGame()
        {
            for (int i = 0; i < miniGameButtons.Length; i++)
            {
                int rnd = UnityEngine.Random.Range(0, miniGameButtons.Length);
                string temp = miniGameButtons[rnd];
                miniGameButtons[rnd] = miniGameButtons[i];
                miniGameButtons[i] = temp;
            }
        }

        public void OnItemStarted(InputAction.CallbackContext context)
        {
            OnItemStarted();
        }

        private void OnItemStarted()
        {
            if (stunnedForSeconds > 0 || !MatchManager.instance.isMatchRunning || currentAmmo <= 0)
            {
                tryingToUseItem = true;
                return;
            }

            tryingToUseItem = false;
            if (team == Team.Dirt)
            {
                m_MyAudioSource.Play();
                InitializeGrenade();
                InitializeGrenadeCommand();
                drawGrenadeCurve = true;
                grenadeThrowStartTime = Time.time;
            }
            else if (team == Team.Clean)
            {
                currentAmmo -= 1;
                PlayWaterHoseSoundCommand();
                UseWaterHose();
            }
        }

        [Command]
        private void PlayWaterHoseSoundCommand()
        {
            PlayWaterHoseSoundRPC();
        }

        [ClientRpc]
        private void PlayWaterHoseSoundRPC()
        {
            GetComponent<AudioScript>().playWaterhose();
        }

        public void OnItemCanceled(InputAction.CallbackContext context)
        {
            tryingToUseItem = false;
            if (stunnedForSeconds > 0 || !MatchManager.instance.isMatchRunning || currentAmmo <= 0)
            {
                return;
            }

            if (team == Team.Dirt)
            {
                grenadeIndicatorLine.positionCount = 0;
                drawGrenadeCurve = false;
                currentAmmo -= 1;
                m_MyAudioSource.Stop();
                GetComponent<AudioScript>().playBombThrow();
                ThrowGrenade(Time.time - grenadeThrowStartTime, -1, -1);
                Destroy(grenade);
            }
        }

        [Command]
        private void PlayBombThrowSoundCommand()
        {
            PlayBombThrowSoundRPC();
        }

        [ClientRpc]
        private void PlayBombThrowSoundRPC()
        {
             PlayBombThrowSoundCommand();
        }

        public override void OnStartAuthority()
        {
            Debug.Log("I have Connected");
            MatchManager.instance.AddPlayer(this);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (hasAuthority)
            {
                if (other.tag.Equals("LobbyView"))
                    MatchManager.instance.cameraController.SlideToLobby();
                else if (other.tag.Equals("TutorialView"))
                    MatchManager.instance.cameraController.SlideToTutorial();
                else if (other.tag.Equals("PreviewView"))
                    MatchManager.instance.cameraController.SlideToView();
                
                else if (other.tag.Equals("TutAttack"))
                    MatchManager.instance.cameraController.SlideToAttackTut();
                else if (other.tag.Equals("TutClean"))
                    MatchManager.instance.cameraController.SlideToInterCleanTut();
                else if (other.tag.Equals("TutDirty"))
                    MatchManager.instance.cameraController.SlideToInterDirtTut();
                
                
                else if (other.tag.Equals("TeamCleanZone"))
                    MatchManager.instance.SetTeamCommand(this, false, true);
                else if (other.tag.Equals("TeamDirtyZone"))
                    MatchManager.instance.SetTeamCommand(this, true, true);
                else if (other.tag.Equals("LobbyZone"))
                    MatchManager.instance.UnsetTeamCommand(this, true);


                if (other.CompareTag("Collectible") && team == Team.Clean)
                {
                    var bookBehavior = other.gameObject.GetComponent<BookBehavior>();
                    other.enabled = false;
                    bookBehavior.Collected(gameObject);
                    inventory.Add(bookBehavior);
                }
            }

            if (isServer && other.gameObject.TryGetComponent(out PowerUp powerUp))
            {
                powerUpForSeconds = powerUp.GetPowerUpDuration();
                NetworkServer.Destroy(powerUp.gameObject);
                MatchManager.instance.PreparePowerUpSpawn();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (hasAuthority)
            {
                if (other.tag.Equals("PreviewView"))
                    MatchManager.instance.cameraController.SlideToLobby();
                else if (other.tag.Equals("TutAttack"))
                    MatchManager.instance.cameraController.SlideToTutorial();
                else if (other.tag.Equals("TutClean"))
                    MatchManager.instance.cameraController.SlideToTutorial();
                else if (other.tag.Equals("TutDirty"))
                    MatchManager.instance.cameraController.SlideToTutorial();
            }
        }


        [Command]
        private void UseWaterHose()
        {
            var waterHose = Instantiate(waterHosePrefab).GetComponent<WaterHose>();
            waterHose.grid = grid;
            waterHose.playerTransform = physicalPlayer;
            NetworkServer.Spawn(waterHose.gameObject);
        }

        private void DrawGrenadeCurve(float chargeTime)
        {
            Vector3[] positions =
                grenade.GetIndicatorLinePoints(physicalPlayer.position, physicalPlayer.forward, chargeTime);
            grenadeIndicatorLine.positionCount = positions.Length;
            grenadeIndicatorLine.SetPositions(positions);
            DrawGrenadeCurveCommand(chargeTime);
        }

        [Command]
        private void DrawGrenadeCurveCommand(float chargeTime)
        {
            //GetComponent<AudioScript>().playBombCharge();
            DrawGrenadeCurveServer(chargeTime);
        }

        private void DrawGrenadeCurveServer(float chargeTime)
        {
            grenade.GetIndicatorLinePoints(physicalPlayer.position, physicalPlayer.forward, chargeTime);
        }

        [Command]
        private void InitializeGrenadeCommand()
        {
            grenade = Instantiate(grenadePrefab).GetComponent<Grenade>();
            grenade.grid = grid;
            grenade.transform.position = physicalPlayer.position;
            grenade.transform.rotation = physicalPlayer.rotation;
            grenade.thrower = gameObject;
            grenade.gameObject.SetActive(false);
            NetworkServer.Spawn(grenade.gameObject);
        }

        private void InitializeGrenade()
        {
            if (grenade == null)
            {
                Destroy(grenade);
            }

            grenade = Instantiate(grenadePrefab).GetComponent<Grenade>();
            grenade.grid = grid;
            grenade.transform.position = physicalPlayer.position;
            grenade.transform.rotation = physicalPlayer.rotation;
            grenade.thrower = gameObject;
            grenade.gameObject.SetActive(false);
        }

        [Command]
        private void ThrowGrenade(float chargeTime, float radius, float scale)
        {
            if (grenade == null)
            {
                InitializeGrenade();
            }

            if (!grenade.HasFlightPath())
            {
                DrawGrenadeCurveServer(chargeTime);
            }

            grenade.gameObject.SetActive(true);
            grenade.thrown = true;
            grenade.chargeTime = chargeTime;
            if (radius >= 0)
            {
                grenade.explosionRadius = radius;
            }

            if (scale >= 0)
            {
                grenade.transform.localScale *= scale;
            }
        }

        [ClientRpc]
        public void Spawn(Transform location)
        {
            if (hasAuthority)
            {
                controller.enabled = false;
                float x = UnityEngine.Random.Range(location.position.x - 5 * location.lossyScale.x,
                    location.position.x + 5 * location.lossyScale.x);
                float z = UnityEngine.Random.Range(location.position.z - 5 * location.lossyScale.z,
                    location.position.z + 5 * location.lossyScale.z);
                transform.position = new Vector3(x, 0.5f, z);
                controller.enabled = true;
            }
        }

        [Command]
        private void SetName(string newName)
        {
            playerName = newName;
        }

        [Command]
        private void SetReady(bool newReady)
        {
            isReady = newReady;
        }

        [ClientRpc]
        public void SetTeam(Team newTeam)
        {
            if (hasAuthority)
            {
                ammoUI.SetTeam(newTeam);
                if (newTeam == Team.Lobby)
                    MatchManager.instance.cameraController.MoveToLobby();
                else
                    MatchManager.instance.cameraController.MoveToLevel();
            }

            team = newTeam;
            if (team == Team.Lobby)
            {
                teamCleanModel.SetActive(false);
                teamDirtyModel.SetActive(false);
                lobbyModel.SetActive(true);
            }
            else if (team == Team.Clean)
            {
                previousPosition = new Vector2(transform.position.x, transform.position.z);
                previousRight = new Vector2(physicalPlayer.right.x, physicalPlayer.right.z).normalized;

                teamCleanModel.SetActive(true);
                teamDirtyModel.SetActive(false);
                lobbyModel.SetActive(false);
                material = cleanMaterial;
            }
            else if (team == Team.Dirt)
            {
                teamCleanModel.SetActive(false);
                teamDirtyModel.SetActive(true);
                lobbyModel.SetActive(false);
                material = dirtyMaterial;
            }
        }

        [ClientRpc]
        private void InvincibilityBlinkingRpc(float time)
        {
            StartCoroutine("InvincibilityBlinkingCoroutine", time);
        }

        private IEnumerator InvincibilityBlinkingCoroutine(float time)
        {
            if (isBlinking)
            {
                yield break;
            }

            isBlinking = true;
            float switchTime = 0.15f * time;
            Texture texture = material.GetTexture("_BaseMap");
            bool isTextureOff = false;
            while (time > 0 && MatchManager.instance.isMatchRunning)
            {
                if (isTextureOff)
                {
                    material.SetTexture("_BaseMap", texture);
                    isTextureOff = false;
                }
                else
                {
                    material.SetTexture("_BaseMap", null);
                    isTextureOff = true;
                }

                yield return new WaitForSeconds(switchTime);
                time -= switchTime + Time.deltaTime;
                switchTime *= 0.85f;
            }

            material.SetTexture("_BaseMap", texture);
            isBlinking = false;
        }
    }
}