using Mirror;
using System;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;

namespace Clirty
{
    [RequireComponent(typeof(CharacterController))]
    public class PlayerMovement : NetworkBehaviour
    {
        [SerializeField] public float playerSpeed = 10.0f;
        private CharacterController controller;
        private Vector3 playerVelocity;
        private Vector2 inputDir;
        private GameObject[] destroyables;
        private NavMeshAgent agent;
        private bool useNav = false;

        private void Start()
        {
            controller = gameObject.GetComponent<CharacterController>();
            agent = GetComponent<NavMeshAgent>();
            if(hasAuthority)
                ConnectToInput();
            destroyables = GameObject.FindGameObjectsWithTag("Destroyable");
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            inputDir = context.ReadValue<Vector2>();
        }

        public void OnMouseClick(InputAction.CallbackContext context)
        {
            //Mouse mouse = Mouse.current;
            //Vector3 mousePos = Mouse.current.position.ReadValue();
            //mousePos.z = Camera.main.transform.position.y;
            //Vector3 worldPoint = Camera.main.ScreenToWorldPoint(mousePos);
            //Vector3 move = worldPoint - transform.position;
            //Debug.Log(worldPoint + " vs " + transform.position);
            //move.y = 0;
            //inputDir.x = move.x;
            //inputDir.y = move.z;

            ////Prevents shaking movement if click is too close to the player
            //if (!(inputDir.magnitude <= 0.1))
            //{
            //    useNav = true;
            //}
            //else
            //{
            //    inputDir = new Vector3(0, this.transform.position.y, 0);
            //    useNav = false;
            //}
        }

        void ConnectToInput()
        {
            ClirtyNetworkManager.ClirtySingleton.AttachToInput("Movement",OnMove);
        }

        void Update()
        {
            if (hasAuthority)
            {
                Vector3 move = new Vector3(inputDir.x, 0, inputDir.y);
                if (!useNav)
                {
                    controller.Move(move * Time.deltaTime * playerSpeed);
                }
                else
                {
                    agent.SetDestination(new Vector3(inputDir.x, 0, inputDir.y));
                }
            }

            GameObject destroyable = GetDestroyableInRadius();

            if(destroyable != null)
            {

            }
        }

        private GameObject GetDestroyableInRadius()
        {
            foreach(GameObject go in destroyables)
            {
                if(Vector3.Distance(transform.position, go.transform.position) < 0.75)
                {
                    return go;
                }
            }
            return null;
        }
    }

//void FixedUpdate()
//{
//    //Gamepad movement
//    Gamepad gamepad = Gamepad.current;

//    if(gamepad != null)
//    {
//        Vector2 move = gamepad.leftStick.ReadValue();
//        Vector3 movement = new Vector3()
//        {
//            x = move.x,
//            y = 0,
//            z = move.y
//        };
//        transform.Translate(movement * speed * Time.deltaTime);
//    }


//    //Mouse movement
//    Mouse mouse = Mouse.current;

//    if (mouse.rightButton.isPressed)
//    {
//        Vector3 mousePos = Mouse.current.position.ReadValue();
//        mousePos.z = Camera.main.transform.position.y;
//        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(mousePos);
//        Vector3 mouseMove = worldPoint - transform.position;
//        Debug.Log(worldPoint + " vs " + transform.position);
//        mouseMove.y = 0;

//        //Prevents shaking movement if click is too close to the player
//        if (!(mouseMove.magnitude <= 0.1))
//        {
//            mouseMove.Normalize();
//            transform.Translate(mouseMove * speed * Time.deltaTime);
//        }

//    }
//}
}