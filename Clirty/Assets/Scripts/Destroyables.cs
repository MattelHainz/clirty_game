using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyables : MonoBehaviour
{
    public enum State
    {
        Fixed,
        Destroyed,
        Modified
    }

    public State state = State.Fixed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Destroy()
    {
        var renderer = GetComponent<Renderer>();
        renderer.material.color = Color.black;
        state = State.Destroyed;
    }

    public void Repair()
    {
        var renderer = GetComponent<Renderer>();
        renderer.material.color = Color.green;
        state = State.Fixed;
    }
}
