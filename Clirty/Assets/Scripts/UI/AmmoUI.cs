using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoUI : MonoBehaviour
{
    [SerializeField] private Color emptyColor;
    [SerializeField] private Color teamCleanColor;
    [SerializeField] private Color teamDirtColor;

    public Color actualColor = Color.white;

    [SerializeField] private GameObject ammoIndicatorPrefab;
    [SerializeField] private float paddingFactor;

    public float maxAmmo;
    public List<RawImage> ammoIndicators;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void SetTeam(Clirty.Player.Team team)
    {
        if(team == Clirty.Player.Team.Clean)
        {
            actualColor = teamCleanColor;
        } else if (team == Clirty.Player.Team.Dirt)
        {
            actualColor = teamDirtColor;
        } else
        {
            actualColor = new Color(0, 0, 0, 0);
        }
    }

    public void SetMaxAmmo(int maxAmmo)
    {
        ammoIndicators = new List<RawImage>(maxAmmo);
        var ammoIndicatorStud = Instantiate(ammoIndicatorPrefab);
        ammoIndicatorStud.transform.SetParent(transform);
        float ammoIndicatorWidth = ammoIndicatorStud.GetComponent<RectTransform>().sizeDelta.x * ammoIndicatorStud.transform.localScale.x * paddingFactor;
        Destroy(ammoIndicatorStud);
        float xPosition = 0 - ammoIndicatorWidth * maxAmmo / 2.0f + ammoIndicatorWidth / 2.0f;
        this.maxAmmo = maxAmmo;
        for(int i = 0; i < maxAmmo; i++)
        {
            var ammoIndicator = Instantiate(ammoIndicatorPrefab).GetComponent<RawImage>();
            ammoIndicator.transform.SetParent(transform);
            var localPosition = ammoIndicator.rectTransform.localPosition;
            localPosition.x = xPosition;
            localPosition.y = 0;
            localPosition.z = 0;
            ammoIndicator.rectTransform.localPosition = localPosition;
            xPosition += ammoIndicatorWidth;
            ammoIndicator.color = emptyColor;
            ammoIndicators.Add(ammoIndicator);
        }
    }

    public void SetCurrentAmmo(int currentAmmo, float fraction)
    {
        foreach(var ammoIndicator in ammoIndicators)
        {
            if(currentAmmo > 0)
            {
                ammoIndicator.color = actualColor;
            } else if (currentAmmo == 0)
            {
                ammoIndicator.color = Color.Lerp(emptyColor, actualColor, fraction * fraction * 0.8f);
            } else
            {
                ammoIndicator.color = emptyColor;
            }
            currentAmmo--;
        }
    }
}
