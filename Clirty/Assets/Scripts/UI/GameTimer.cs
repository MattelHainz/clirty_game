using System;
using System.Collections;
using System.Collections.Generic;
using Clirty;
using Mirror;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour
{
    private Text text;

    public void Start()
    {
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        int time = (int)Math.Floor(NetworkTime.time);
        if (time < 0)
            time = 0;
        int seconds = time % 60;
        int minutes = time / 60;
        text.text = minutes + ":" + seconds;
    }
}
