using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIRotater : MonoBehaviour
{
    [SerializeField] private float rotationsPerSecond;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.back, 360 * rotationsPerSecond * Time.deltaTime );
    }
}
