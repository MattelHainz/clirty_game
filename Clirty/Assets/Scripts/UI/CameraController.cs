using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform lobbyAnchor, levelAnchor, tutorialAnchor, viewAnchor, tutorialAttackAnchor, tutorialInterDirtyAnchor, tutorialInterCleanAnchor;

    private IEnumerator slide;
    public float slideDuration = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        MoveToLobby();
    }

    public void MoveToLobby()
    {
        if (slide != null)
            StopCoroutine(slide);
        transform.position = lobbyAnchor.position;
        transform.rotation = lobbyAnchor.rotation;
    }

    public void MoveToLevel()
    {
        if (slide != null)
            StopCoroutine(slide);
        transform.position = levelAnchor.position;
        transform.rotation = levelAnchor.rotation;
    }

    public void SlideToTutorial()
    {
        if (slide != null)
            StopCoroutine(slide);
        slide = SlideCoroutine(tutorialAnchor);
        StartCoroutine(slide);
    }

    public void SlideToLobby()
    {
        if (slide != null)
            StopCoroutine(slide);
        slide = SlideCoroutine(lobbyAnchor);
        StartCoroutine(slide);
    }    
    public void SlideToView()
    {
        if (slide != null)
            StopCoroutine(slide);
        slide = SlideCoroutine(viewAnchor);
        StartCoroutine(slide);
    }
    
    public void SlideToAttackTut()
    {
        if (slide != null)
            StopCoroutine(slide);
        slide = SlideCoroutine(tutorialAttackAnchor);
        StartCoroutine(slide);
    }
    
    public void SlideToInterDirtTut()
    {
        if (slide != null)
            StopCoroutine(slide);
        slide = SlideCoroutine(tutorialInterDirtyAnchor);
        StartCoroutine(slide);
    }
    
    public void SlideToInterCleanTut()
    {
        if (slide != null)
            StopCoroutine(slide);
        slide = SlideCoroutine(tutorialInterCleanAnchor);
        StartCoroutine(slide);
    }

    private IEnumerator SlideCoroutine(Transform to)
    {
        float startTime = Time.time;
        Vector3 startPosition = transform.position;
        Quaternion startRotation = transform.rotation;
        while (Time.time <= startTime + slideDuration)
        {
            float progress = (Time.time - startTime) / slideDuration;
            transform.position = Vector3.Lerp(startPosition, to.position, progress);
            transform.rotation = Quaternion.Lerp(startRotation, to.rotation, progress);
            yield return new WaitForEndOfFrame();
        }

        transform.position = to.position;
        transform.rotation = to.rotation;
    }
}