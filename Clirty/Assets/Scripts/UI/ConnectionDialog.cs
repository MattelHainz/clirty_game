using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.UI;

namespace Clirty.UI
{
    
public class ConnectionDialog : MonoBehaviour
{
    [SerializeField] private GameObject connectingDisplay;
    [SerializeField] private GameObject disconnectedDisplay;

    [SerializeField] private Button button;
    [SerializeField] Text buttonText;

    [SerializeField] private float dotSpeedPerSecond;
    [SerializeField] private Text nameInputText;
    [SerializeField] private Text ipInputText;
    

    void Update()
    {
        disconnectedDisplay.SetActive(false);
        connectingDisplay.SetActive(false);
        button.interactable = true;
        buttonText.text = "Connect";

        if (NetworkClient.isConnecting)
        {
            button.interactable = false;
            connectingDisplay.SetActive(true);
            if (Time.time % dotSpeedPerSecond < dotSpeedPerSecond / 3.0f)
                buttonText.text = "Connecting.";
            else if (Time.time % dotSpeedPerSecond < 2.0f * (dotSpeedPerSecond / 3.0f))
                buttonText.text = "Connecting..";
            else
                buttonText.text = "Connecting...";
        
        }
        else if (ClirtyNetworkManager.lastConnectionLost)
        {
            disconnectedDisplay.SetActive(true);
        }
    }
    
    public void Connect()
    {
        NetworkManager.singleton.networkAddress = ipInputText.text;
        ClirtyNetworkManager.ClirtySingleton.localPlayerName = nameInputText.text;
        #if UNITY_EDITOR
        ClirtyNetworkManager.ClirtySingleton.StartHost();
        #else
        NetworkManager.singleton.StartClient();
        #endif
        
    }
}
}
