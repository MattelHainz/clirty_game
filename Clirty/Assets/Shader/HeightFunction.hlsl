static const int SIZE = 9;

float3 GetNormal(UnityTexture2D textureStruct, float2 uv) {
	float L = SAMPLE_TEXTURE2D_LOD(textureStruct, sampler_BaseMap, uv - float2(_TexelSize.x,0), 0)* _Height;
	float R = SAMPLE_TEXTURE2D_LOD(textureStruct, sampler_BaseMap, uv + float2(_TexelSize.x, 0), 0)* _Height;
	float T = SAMPLE_TEXTURE2D_LOD(textureStruct, sampler_BaseMap, uv - float2(0, _TexelSize.y), 0)* _Height;
	float B = SAMPLE_TEXTURE2D_LOD(textureStruct, sampler_BaseMap, uv + float2(0, _TexelSize.y), 0)* _Height;
	float3 normal = float3((L -R) * 0.5f, 1, (T - B) * 0.5f);
	normal = normalize(normal);
	return normal;
}

void HeightFunction_float(float4 uv, out float height, out float3 normal) {
	UnityTexture2D textureStruct = UnityBuildTexture2DStructNoScale(_BaseMap);
	/*if (SAMPLE_TEXTURE2D_LOD(textureStruct, sampler_BaseMap, uv.xy, 0).r < 0.25) {
		height = 0;
		normal = float3(0, 1, 0);
		return;
	}*/

	normal = GetNormal(textureStruct, uv.xy);

	int halfSize = SIZE / 2;
	uv.xy -= halfSize * _TexelSize;
	int dirty = 0;
	int total = 0;
	for (int x = 0; x < SIZE; x++) {
		for (int y = 0; y < SIZE; y++) {
			float2 currentUV = uv.xy + _TexelSize * float2(x, y);
			if (currentUV.x < 0 || currentUV.x > 1 || currentUV.y < 0 || currentUV.y > 1) {
				continue;
			}
			total++;
			dirty += SAMPLE_TEXTURE2D_LOD(textureStruct, sampler_BaseMap, currentUV.xy, 0).r;
		}
	}
	height = dirty * 1.0f / total *_Height;
	//height = (SAMPLE_TEXTURE2D_LOD(textureStruct, sampler_BaseMap, uv, 0).r > 0.5) * _Height;
}